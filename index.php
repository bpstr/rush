<?php
error_reporting(-1);
ini_set('display_startup_errors', 1);
ini_set('display_errors', 'On');
error_reporting(E_ALL);
header('Content-type: text/html; charset=utf-8');
header( 'Cache-Control: max-age=2592000, public' );
$dev = true;/*
if((empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") && isset($_SERVER['HTTP_HOST']) && isset($_SERVER['REQUEST_URI'])){
	$redirect= "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	Header("HTTP/1.1 301 Moved Permanently");
	header("Location:$redirect");
	exit();
}*/
?>
<!DOCTYPE html>
<html lang="hu">
<head>
	<!--
		<?php
			include "maincore.php";
		?>
	-->

	<script type="application/ld+json">
		{
		  "@context": "http://schema.org",
		  "@type": "Organization",
		  "url": "<?php echo $page->setting("address"); ?>",
		  "name": "<?php echo $page->setting("sitename"); ?>",
		  "contactPoint": {
			"@type": "ContactPoint",
			"telephone": "<?php echo $page->setting("phone"); ?>",
			"contactType": "Customer service"
		  }
		}
	</script>

	<base href="<?php echo $CNF->adress; ?>">
	<title><?php echo $page->name; ?> - <?php echo $page->setting("sitename"); ?></title>

	<!-- ICONS -->
	<?php /* include "includes/favicons/icons.txt"; */ ?>

	<!-- META -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="robots" content="INDEX, FOLLOW">
	<meta name="revisit-after" content="1 days" />
	<meta name="rating" content="general" />
	<meta name="author" content="<?php echo $page->setting("author"); ?>" />
	<meta name="description" content="<?php echo $page->desc; ?>">

	<!-- CSS  -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" crossorigin="anonymous">
	<link rel="stylesheet" href="assets/css/kravmaga.css?t=<?php echo time();?>" type="text/css"/>

	<meta property="og:title" content="<?php echo $page->name; ?> - <?php echo $page->setting("sitename"); ?>">
	<meta property="og:site_name" content="<?php echo $page->setting("sitename"); ?>">
	<meta property="og:url" content="<?php echo $CNF->adress . $page->current; ?>">
	<meta property="og:description" content="<?php echo $page->desc; ?>">
	<meta property="og:type" content="article">
	<meta property="og:image" content="***">

</head>

<body>

	<!-- Navigation -->
    <nav id="mainNavigation" class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                <img src="assets/KMG.svg" height="30" class="d-inline-block align-top" alt="">
                <span class="d-inline-block text-primary blackops position-relative font-weight-bold">Óber Tamás<small>Kravmaga instruktor</small></span>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto my-2 my-lg-0 px-3">
					<?php 
						foreach ($page->menu as $row) {
							echo '<li class="nav-item"><a href="http://www.onvedelem-kravmaga.hu/test/index.php?url='.$row["uri"].'" class="nav-link font-weight-bold">'.$row["name"].'</a></li>';
						}
						echo '<li class="nav-item"><a href="'.$page->setting('facebook').'" target="_blank" title="Facebook oldalunk" class="nav-link js-scroll-trigger"><img src="https://nempiskota.eu/images/social/iconmonstr-facebook-6.svg"><span class="d-lg-none font-weight-bold ml-2">Facebook oldalunk</span></a></li>';
					?>
				</ul>
			</div>
		</div>
	</nav>

	<!-- Content -->
	<?php include $page->loadPage(); ?>

	<!-- Footer -->
	<footer class="bg-dark mt-5 pb-5">
		<iframe class="z-depth-1" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2694.993567974047!2d19.168409415317463!3d47.50951647917826!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4741c4bcaf1f65ef%3A0x2af5b8cdb4ed252b!2zTmVtUGlza8OzdGEgw4l0a2V6ZGU!5e0!3m2!1shu!2shu!4v1510589137394" width="100%" height="420px" frameborder="0" style="border:0" allowfullscreen></iframe>
		<div class="container">
			<div class="row py-3 text-light">
				<?php
					foreach($page->textGroup("features") as $row) {
						echo '<div class="col-lg-3 col-md-6 text-center">';
						echo '<div class="">';
						echo '<h2 class="center text-primary"><i class="material-icons">'.$row["icon"].'</i></h2>';
						echo '<h5 class="center">'.$row["title"].'</h5>';
						echo '<p class="center flow-text">'.$row["content"].'</p>';
						echo '</div>';
						// echo '</a>';
						echo '</div>';
					}
				?>
			</div>
		</div>
	</footer>

	<!--  Scripts-->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>

</body>
</html>
