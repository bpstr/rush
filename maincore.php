<?php
// Set variables
setLocale(LC_CTYPE, 'HU_hu.UTF-8');
error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

session_start();

// Include files
include "config.php";
include "pagemanager.php";
include "statmanager.php";

// Create classes
$CNF = new Config ();

// Create connection
$conn = new mysqli($CNF->ml[0], $CNF->ml[1], $CNF->ml[2], $CNF->ml[3]);
if ($conn->connect_error) die("Database connection failed: " . $conn->connect_error);
if (!$conn->set_charset("utf8")) printf("Err w/ charset: %s\n", $conn->error);

// Create page
$page = new PageManager($conn, array(1, (isset($_GET['url'])) ? $_GET["url"] : "fooldal"));

// Create statistics
$stats = new StatManager($conn);

// visitor tracking
$log = true;
if (isset($_GET["url"]) || $page->current != '') {
	$campaign = isset($_SESSION["campaign"]) ? $_SESSION["campaign"] : "";
	$uri = (isset($_GET["url"])) ? $_GET["url"] : $page->current;
	// $location = $_SESSION["location_info"];
	$_SESSION["isMobile"] = $stats->isMobile();
	if (strpos($uri, ".png") !== false) $log = false;
	if (strpos($uri, "favicon.ico") !== false) $log = false;
	if (strpos($uri, "fonts/roboto") !== false) $log = false;
	if ($log) {
		$conn->query("INSERT INTO `krav__visitor_tracking` (`id`, `ipaddress`, `date`, `uri`, `from`, `useragent`, `mobile`, `bot`, `pingback`) VALUES (NULL, '".$stats->clientIp."', CURRENT_TIMESTAMP, '".$uri."', '".$conn->escape_string($_SERVER["HTTP_REFERER"])."', '".$conn->escape_string($_SERVER['HTTP_USER_AGENT'])."', '".$_SESSION["isMobile"]."', '".$stats->isBot()."', '');");
	}

}


?>
