<?php
header('Content-type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html lang="hu">
<head>
	<!--
	<?php include "admincore.php"; ?>
	-->

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
	<title><?php echo $page->name; ?> - <?php echo $page->setting("sitename"); ?> Admin</title>


	<!-- CSS  -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">


	<!-- JS  -->
  	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>


  </head>
  <body>


	<nav class="navbar navbar-expand-md navbar-dark bg-dark">
		<?php
			$brand = array_shift($page->menu);
			echo '<a class="navbar-brand" href="index.php">';
			echo '<b>'.$page->setting("sitename").'</b> Admin';
			echo '</a>';
		?>

	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
		<ul class="navbar-nav mr-auto">
			<?php
				foreach ($page->menu as $row) {
					$active = ($row['file'] == $page->current) ? 'active' : '';
					echo '<li class="nav-item '.$active.'">';
					echo '<a href="index.php?p='.$row["file"].'" class="nav-link">'.$row["name"].'</a>';
					echo '</li>';
				}
			?>
		</ul>
		<span class="navbar-text">
			<a href="<?php echo $CNF->adress; ?>" target="_blank" class="btn btn-sm btn-outline-secondary">Oldal megtekintése</a>
		</span>
	  </div>
	</nav>

	<main role="main" class="py-5">
		<div class="container">
		<div class="row">
			<h1 class="col s12 light white-text"><?php echo $page->name; ?></h1>
		</div>
		<hr>

		<?php include $page->loadPage(); ?>
	</main>

	<footer>
		<div class="container text-center">
			<span class="text-muted">bpstr@gmx.tm | <a href="http://www.rooter.hu/phpmyadmin-1/index.php" target="_blank">phpMyAdmin</a> | <a href="https://www.rackhost.hu/site/login" target="_blank">Rackhost</a></span>
		</div>
	</footer>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

  </body>
</html>
