<?php
error_reporting(-1);
ini_set('display_startup_errors', 1);
ini_set('upload_max_filesize', '8M');
ini_set('post_max_size', '8M');

ini_set('display_errors', 'On');
error_reporting(E_ALL);
// Set variables
setLocale(LC_CTYPE, 'HU_hu.UTF-8');
error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

define('IMAGES_ROOT', '/home/weblapok/www.onvedelem-kravmaga.hu/html/test/');

$_SESSION["isMobile"] =  preg_match("/(android|webos|avantgo|iphone|ipad|ipod|blackbe‌​rry|iemobile|bolt|bo‌​ost|cricket|docomo|f‌​one|hiptop|mini|oper‌​a mini|kitkat|mobi|palm|phone|pie|tablet|up\.browser|up\.link|‌​webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
settype($_SESSION["isMobile"], "boolean");
session_start();

// Include files
include "../config.php";
include "../pagemanager.php";

// Create classes
$CNF = new Config ();


// Create connection
$conn = new mysqli($CNF->ml[0], $CNF->ml[1], $CNF->ml[2], $CNF->ml[3]);
if ($conn->connect_error) die("Database connection failed: " . $conn->connect_error);
if (!$conn->set_charset("utf8")) printf("Err w/ charset: %s\n", $conn->error);

if (isset($_GET["logout"])) {
	unset($_SESSION["admin"]);
}

// Create page
if (!isset($_GET["p"]) && isset($_POST["p"])) $_GET = array("p" => $_POST["p"]);
if (!isset($_GET["p"])) $_GET = array("p" => "dashboard");
$page = new PageManager($conn, array(NULL, $_GET['p']));



if (isset($_POST["user"]) && isset($_POST["pass"])) {
	if ($_POST["user"] == "kravmaga" && $_POST["pass"] == "asd") {
		$_SESSION["admin"] = true;
		$CNF->notify("Admin logged in from ".$CNF->client_ip(), 24);
	} else {
		$ERRORS = "Nem megfelelő felhasználónév vagy jelszó!";
		include "login.html";
		exit;
	}
} elseif (!isset($_SESSION["admin"])) {
	include "login.html";
	exit;
}


function uploadImage($target_file, $target, $fileToField = 'fileToUpload') {
	global $_POST;
	global $_FILES;

	$imgerr = "";
	if (!empty($_FILES[$fileToField]["tmp_name"])) {
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		$check = getimagesize($_FILES[$fileToField]["tmp_name"]);
		if ($check === false) {
			$uploadOk = 0;
			$imgerr = "Nem jó a feltöltött kép";
		}
		// Check if file already exists
		if (file_exists($target)) {
			unlink($target);
			$imgerr = "A feltölteni kívánt kép már létezik";
			// $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
			$imgerr = "Hibás formátum";
			$uploadOk = 0;
		}
		
		if (!is_dir(dirname($target))) {
			var_dump(dirname($target));
		}
		
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk != 0) {
			if (move_uploaded_file($_FILES[$fileToField]["tmp_name"], IMAGES_ROOT . $target)) {
				$uploadOk = 1;
				echo '<div class="alert alert-success" role="alert"><b>'.basename($target).'</b> nevű kép sikeresen feltöltve!</div>';

				$scaled = IMAGES_ROOT . dirname($target) . '/scaled-' . basename($target);
				$cropped = IMAGES_ROOT . dirname($target) . '/cropped-' . basename($target);

				resizeImage(IMAGES_ROOT.$target, $scaled, 1280, 1280);
				smartCrop($scaled, $cropped, 512, 512);



				return basename($target);
			} else {
				$imgerr = "Nem sikerült feltölteni a képet!";
				$uploadOk = 0;
			}
		} else {
			die($imgerr);
		}
	}

}

function watermarkImage ($target) {
	// Load the stamp and the photo to apply the watermark to
	$small = imagecreatefrompng('../images/page/logo-watermark-small.png');
	// $medium = imagecreatefrompng('../images/page/logo-watermark-med.png');
	$large = imagecreatefrompng('../images/page/logo-watermark.png');


    $size=getimagesize($target);
    switch($size["mime"]){
        case "image/jpeg":
            $im = imagecreatefromjpeg($target); //jpeg file
        break;
        case "image/gif":
            $im = imagecreatefromgif($target); //gif file
      break;
      case "image/png":
          $im = imagecreatefrompng($target); //png file
      break;
    default:
        $im=false;
    break;
    }

	// $im = imagecreatefromjpeg($target);


	// Set the margins for the stamp and get the height/width of the stamp image
	$marge_right = 10;
	$marge_bottom = 10;
	$stamp = (imagesx($im) > 2000) ? $large : $small;
	$sx = imagesx($stamp);
	$sy = imagesy($stamp);

	// Copy the stamp image onto our photo using the margin offsets and the photo
	// width to calculate positioning of the stamp.
	imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));

	// Output and free memory
	// header('Content-type: image/png');
	imagejpeg($im, $target);
	imagedestroy($im);
}

function smartcrop($source, $target, $width, $height) {
	require_once('smartcrop/smartcrop.php');

	$smartcrop = new xymak\image\smartcrop($source,[
		'width' => $width,
		'height' => $height
	]);
	//Analyse the image and get the optimal crop scheme
	$res = $smartcrop->analyse();
	//Generate a crop based on optimal crop scheme
	$smartcrop->crop($res['topCrop']['x'], $res['topCrop']['y'], $res['topCrop']['width'], $res['topCrop']['height']);
	//Output the image

	imagejpeg($smartcrop->get(), $target);
}


function resizeImage($sourceImage, $targetImage, $maxWidth, $maxHeight, $quality = 80) {
	$isValid = getimagesize($sourceImage);

	if (!$isValid)
	{
		return false;
	}

	// Get dimensions and type of source image.
	list($origWidth, $origHeight, $type) = getimagesize($sourceImage);

	if ($maxWidth == 0)
	{
		$maxWidth  = $origWidth;
	}

	if ($maxHeight == 0)
	{
		$maxHeight = $origHeight;
	}

	// Calculate ratio of desired maximum sizes and original sizes.
	$widthRatio = $maxWidth / $origWidth;
	$heightRatio = $maxHeight / $origHeight;

	// Ratio used for calculating new image dimensions.
	$ratio = min($widthRatio, $heightRatio);

	// Calculate new image dimensions.
	$newWidth  = (int)$origWidth  * $ratio;
	$newHeight = (int)$origHeight * $ratio;

	// Create final image with new dimensions.
	$newImage = imagecreatetruecolor($newWidth, $newHeight);

	// Obtain image from given source file.
	switch(strtolower(image_type_to_mime_type($type)))
	{
		case 'image/jpeg':
			$image = @imagecreatefromjpeg($sourceImage);
			if (!$image)
			{
				return false;
			}

			imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);

			if(imagejpeg($newImage,$targetImage,$quality))
			{
				// Free up the memory.
				imagedestroy($image);
				imagedestroy($newImage);
				return true;
			}
			break;

		case 'image/png':
			$image = @imagecreatefrompng($sourceImage);

			if (!$image)
			{
				return false;
			}

			imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);

			if(imagepng($newImage,$targetImage, floor($quality / 10)))
			{
				// Free up the memory.
				imagedestroy($image);
				imagedestroy($newImage);
				return true;
			}
			break;

		default:
			return false;
	}
}

?>
