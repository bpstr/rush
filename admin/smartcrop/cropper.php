<?php
error_reporting(-1);
ini_set('display_startup_errors', 1);
ini_set('display_errors', 'On');
error_reporting(E_ALL);

require(__DIR__ . '/smartcrop.php');
use xymak\image;
//Get a instance
$smartcrop = new xymak\image\smartcrop('cropit.jpg',[
	'width' => 512, 
	'height' => 512
]);
//Analyse the image and get the optimal crop scheme
$res = $smartcrop->analyse();
//Generate a crop based on optimal crop scheme
$smartcrop->crop($res['topCrop']['x'], $res['topCrop']['y'], $res['topCrop']['width'], $res['topCrop']['height']);
//Output the image

imagejpeg($smartcrop->get(), 'simpletext.jpg'); 