<?php

if (isset($_POST["edit"]) && isset($_POST["value"])) {
	$value = $conn->escape_string($_POST["value"]);
	if (ctype_digit($_POST["edit"]) && $_POST["edit"] > 0) {
		$conn->query("UPDATE `krav__settings` SET `value` = '$value' WHERE `id` = ".$_POST["edit"]);
	}
}

?>


<?php if (isset($_GET["id"])) {
		$id = (ctype_digit($_GET["id"])) ? $_GET["id"]: 0;
		$row = $page->fetch("SELECT * FROM krav__settings WHERE id = $id", false);
?>


<div class="row">
	<h3 class="col"><?php echo $row["name"]; ?></h3>
</div>

<form method="POST" action="index.php" autocomplete="off">
	<div class="row">
		<div class="form-group col">
			<input id="value" type="text" name="value" class="form-control" value="<?php echo $row["value"]; ?>">
		</div>
		<div class="form-group col">
			<input type="hidden" name="edit" value="<?php echo $id; ?>">
			<input type="hidden" name="p" value="<?php echo $page->current; ?>">
			<button class="btn btn-primary btn-block" type="submit" name="action" value="0">Mentés</button>
		</div>
	</div>
</form>

<?php } ?>
<div class="row">
	<div class="col s12">
			<?php
				$group = "";
				foreach($page->fetch("SELECT * FROM krav__settings") as $row) {
					if ($group != $row["type"]) {
						if (!empty($group)) echo '</div>';
						echo '<h5>'.$CNF->settingNames[$row["type"]].' beállítások</h5>';
						echo '<div class="list-group my-3">';
						$group = $row["type"];
					}
					echo '<a href="index.php?p=settings_board&id='.$row["id"].'" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">';

					echo '<span>'.$row["name"].'  <code>{'.$row["uri"].'}</code></span><b class="text-nowrap text-truncate" style="max-width: 50%">'.$row["value"].'</b>';
					echo '</a>';
				}
			?>
		</div>
	</div>
</div>
