<div class="row">
	<h2 class="col s12 light white-text"><?php echo $page->name; ?></h2>
</div>
<?php $page->subpages(); ?>
<div class="row">
	<div class="col s12 m6">
		<ul class="collection with-header">
			<li class="collection-header"><h4>Legutóbbi feliratkozók</h4></li>
			<?php 
				foreach($page->fetch("SELECT * FROM newsletter ORDER BY date DESC LIMIT 0,20") as $row) {
					$icon = ($row["is_allowed"] == "0") ? "&#xE7FF;" : "&#xE7FD;";
					$color = ($row["is_allowed"] == "0") ? "blue-grey" : "light-green";
					echo '<a href="index.php?p=pages_manage&id='.$row["id"].'" class="collection-item avatar blue-grey-text">';
					echo '<i class="material-icons circle '.$color.' white-text">'.$icon.'</i>';
					echo '<span class="title">'.$row["name"].'</span>';
					echo '<p class="truncate">'.$row["email"].' &middot; '.$CNF->ago($row["date"]).'</p>';
					echo '</a>';
				}
			?>			
		</ul>
	</div>
	<div class="col s12 m6">
		<ul class="collection with-header">
			<li class="collection-header"><h4>Kiküldött levelek</h4></li>
			<?php 
				foreach($page->fetch("SELECT * FROM mail_sent") as $row) {
					echo '<a href="index.php?p=pages_articles&id='.$row["id"].'" class="collection-item avatar blue-grey-text">';
					echo '<i class="material-icons circle blue white-text">'.$row["icon"].'</i>';
					echo '<span class="title">'.$row["title"].'</span>';
					echo '<p class="truncate">'.$row["content"].'</p>';
					echo '</a>';
				}
			?>
		</ul>
	</div>
</div> 