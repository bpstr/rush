<?php 
$page->breadcrump(); 


if (isset($_GET["delete"])) {
	if (ctype_digit($_GET["delete"]) && $_GET["delete"] > 0) {
		$result = $conn->query("DELETE FROM `galleries` WHERE `id` = ".$_GET["delete"]) or die($conn->error);
	}
}

if (isset($_POST["edit"])) {
	$gallery = array("name" => "", "uri" => "", "image" => "");
	foreach($_POST as $key => $value) {
		if (!in_array($key, array_keys($gallery))) continue;
		$gallery[$key] = $conn->escape_string($value);
	}
	$gallery["uri"] = $CNF->toAscii(trim($gallery["name"]));
	
	$gallery["image"] = uploadImage("../images/gallery/" . basename($_FILES["fileToUpload"]["name"]), "../images/gallery/".date("y-m-d")."-".$gallery["uri"].".jpg");
	if ($gallery["image"] == NULL) $TOAST = "A képet nem sikerült feltölteni, mert túl nagy. Maximum méret: 2Mb";
	
	if ($_POST["edit"] == 0) {
		$result = $conn->query("INSERT INTO `galleries` (`id`, `name`, `uri`, `image`) VALUES (NULL, '".implode("', '", $gallery)."');");
		if ($result) {
			$TOAST = "Sikeresen létrehoztuk a galériát!";
		} else {
			$TOAST = array($conn->error, "Már létezik ilyen galéria!");
		}
	} elseif(ctype_digit($_POST["edit"])) {
		$q = array();
		foreach($gallery as $key => $value) {
			$q[] = "`$key` = '$value'";
		} 
		$TOAST = "Sikeresen frissítettük a galériát!";
		$result = $conn->query("UPDATE `galleries` SET ".implode(", ", $q)." WHERE id = ".$_POST["edit"]) or die($conn->error);
	}
	var_dump($gallery);
}



if (isset($_GET["id"])) {
	$id = (ctype_digit($_GET["id"])) ? $_GET["id"]: 0;
	if ($id != 0) {
		$row = $page->fetch("SELECT * FROM galleries WHERE id = $id", false);
	} else {
		$row = array();
	}
?>
<div class="row">
	<h3 class="col s12 light white-text">Galéria <?php echo ($id == 0) ? "létrehozása" : "szerkesztése"; ?></h3>
</div>
<div class="row">
	<div class="col s12">
		<div class="card">
            <div class="card-content">
				<span class="card-title">Galéria neve</span>
				<form method="POST" action="index.php" autocomplete="off" enctype="multipart/form-data">
					<div class="row">
						<div class="input-field col s12">
							<input id="name" name="name" type="text" class="validate" value="<?php echo (isset($row["name"])) ? $row["name"] : ""; ?>">
							<label for="name">Név</label>
						</div>
					</div>
					<span class="card-title">Galéria borítója</span>
					<div class="row">
						<?php 
							$colsize = "s12"; 
							if (isset($row["image"])) {
								if ($row["image"] != "") { 
									echo '<div class="file-field input-field col s12 m3">';
									echo '<a href="'.$CNF->adress.'images/gallery/'.$row["image"].'" target="_blank">';
									echo '<img src="../images/gallery/'.$row["image"].'" style="height: 96px">';
									echo '</a>';
									echo '</div>';
									$colsize = "s12 m9";
								}
							}
						?>
						<div class="file-field input-field col <?php echo $colsize; ?>">
							<span class="card-title">Kép feltöltése</span>
							<div class="btn orange darken-2">
								<span>Kiválasztás</span>
								<input type="file" name="fileToUpload" id="fileToUpload" accept="image/*">
							</div>
							<div class="file-path-wrapper">
								<input class="file-path validate" name="filepath" type="text">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12 m6 push-m6">
							<input type="hidden" name="edit" value="<?php echo $id; ?>">
							<input type="hidden" name="p" value="<?php echo $page->current; ?>">
							<button class="btn waves-effect waves-light right orange" type="submit" name="action" value="0">Mentés<i class="material-icons right">send</i></button>
						</div>
						<div class="input-field col s12 m6 pull-m6">
							<a class="waves-effect waves-light btn red darken-2 confirm" href="index.php?p=<?php echo $page->current; ?>&delete=<?php echo $id; ?>"><i class="material-icons left">delete</i>Törlés</a>
						</div>
					</div>
				</form> 
			</div> 
        </div>
	</div>
</div> 
<?php
} else {
?>
<div class="row">
	<h3 class="col s12 m8 l10 light white-text">Galériák</h3> 
</div> 
	<div class="col s12 m6">
		<ul class="collection with-header"> 
			<li class="collection-header"><i class="material-icons left">&#xE3B3;</i><h4>Galériák</h4></li>
			<li class="collection-item"><a href="index.php?p=<?php echo $page->current; ?>&id=0">Új galéria hozzáadása<span class="secondary-content orange-text"><i class="material-icons">&#xE146;</i></span></a></li>
			<?php 
				foreach($page->fetch("SELECT * FROM galleries") as $row) {
					echo '<a href="index.php?p=gallery_edit&id='.$row["uri"].'" class="collection-item avatar blue-grey-text">';
					echo '<img src="../images/gallery/'.$row["image"].'" alt="" class="circle">';
					echo '<span class="title">'.$row["name"].'</span>';
					echo '<p></p>';
					echo '</a>'; 
					$images = $page->fetch("SELECT * FROM images WHERE GID = ".$row["id"]);
					if ($page->fetchedRows > 0) {
						echo '<div class="white" style="height: 100px;">';
						foreach($images as $r) {
							echo '<a href="index.php?p=gallery_image&id='.$r["id"].'">';
							echo '<img src="../images/gallery/'.$r["image"].'" style="height: 100px;" class="left">';
							echo '</a>';
						}
						echo '</div>';
					} else {
						echo '<li class="collection-item"><div>Nincs kép a galériában! <a href="index.php?p=gallery_image&id=0&GID='.$row["id"].'" class="secondary-content">Új kép feltöltése<i class="material-icons right">&#xE439;</i></a></div></li>';
					} 
				}
				if ($page->fetchedRows > 5) {
					echo '<li class="collection-item"><a href="index.php?p=gallery_edit&id=0">Új galéria hozzáadása<span class="secondary-content orange-text"><i class="material-icons">&#xE146;</i></span></a></li>';
				}
			?>			
		</ul>
	</div>
	
<?php } ?>