<?php
if (isset($_GET["delete"])) {
	if (ctype_digit($_GET["delete"]) && $_GET["delete"] > 0) {
		$result = $conn->query("DELETE FROM `krav__trainings` WHERE `id` = ".$_GET["delete"]) or die($conn->error);
		$result = $conn->query("DELETE FROM `krav__training_times` WHERE `TID` = ".$_GET["delete"]) or die($conn->error);
	}
}

if (isset($_POST["edit"])) {
	$content = array("name" => "", "location" => "", "comment" => "", "image" => "");
	foreach($_POST as $key => $value) {
		if (!in_array($key, array_keys($content))) continue;

		$content[$key] = $conn->escape_string($value);
	}

	$uri = $CNF->toAscii(trim($content["name"]));

	$content["image"] = uploadImage("../images/trainings/" . basename($_FILES["fileToUpload"]["name"]), "../images/trainings/".date("y-m-d")."-".$uri.".jpg");

	if ($_POST["edit"] == 0) {
		$result = $conn->query("INSERT INTO `krav__trainings` (`id`, `title`, `location`, `comment`, `image`) VALUES (NULL, '".implode("', '", $content)."');") or die($conn->error);
		if ($result) {
			echo '<div class="alert alert-success" role="alert"><b>'.$content['name'].'</b> sikeresen hozzáadva!</div>';
		}
	} elseif(ctype_digit($_POST["edit"])) {
		$q = array();
		foreach($content as $key => $value) {
			$q[] = "`$key` = '$value'";
		}

		$result = $conn->query("UPDATE `krav__trainings` SET ".implode(", ", $q)." WHERE id = ".$_POST["edit"]) or die($conn->error);
		echo '<div class="alert alert-success" role="alert"><b>'.$content['name'].'</b> sikeresen frissítve!</div>';
	}


}


if (isset($_GET["id"])) {
	$id = (ctype_digit($_GET["id"])) ? $_GET["id"]: 0;
	if ($id != 0) {
		$row = $page->fetch("SELECT * FROM menu WHERE id = $id", false);
	} else {

		$row = array();
	}
?>
<div class="row">
	<h3 class="col">Edzés <?php echo ($id == 0) ? "hozzáadása" : "szerkesztése"; ?></h3>
</div>
<form method="POST" action="index.php" autocomplete="off" enctype="multipart/form-data">
	<div class="row">
		<div class="form-group col">
			<label for="name">Edzés megnevezése</label>
			<input id="name" name="name" type="text" class="form-control" value="<?php echo (isset($row["name"])) ? $row["name"] : ""; ?>">
			<small class="form-text text-muted">Ez jelenik meg vastag betűvel.</small>
		</div>
		<div class="form-group col">
			<label for="location">Edzés helyszíne</label>
			<input id="location" name="location" type="text" class="form-control" value="<?php echo (isset($row["location"])) ? $row["location"] : ""; ?>">
			<small class="form-text text-muted">Az edzés neve alatt jelenik meg.</small>
		</div>
	</div>
    <div class="row">
        <div class="form-group col">
            <label for="comment">Edzés leírása</label>
            <textarea id="comment" name="comment" class="form-control"><?php echo (isset($row["comment"])) ? $row["comment"] : ""; ?></textarea>
        </div>
	</div>

	<div class="row">
		<div class="form-group col">
			<span class="card-title">Kép az edzésről</span>
			<?php
				$colsize = "s12";
				if (isset($row["image"])) {
					if ($row["image"] != "") {
						echo '<div class="file-field input-field col s12 m3">';
						echo '<a href="'.$CNF->adress.'images/trainings/'.$row["image"].'" target="_blank">';
						echo '<img src="../images/trainings/'.$row["image"].'" style="height: 96px">';
						echo '</a>';
						echo '</div>';
					}
				}
			?>
		</div>
		<div class="form-group col">
			<label for="fileToUpload">Kép feltöltése</label>
			<input type="file" name="fileToUpload" id="fileToUpload" accept="image/*" class="form-control-file">
		</div>
	</div>
    <hr>
	<div class="row">
        <h4 class="col">Időpontok</h4>
	</div>
	<div class="row">
		<?php foreach($page->fetch("SELECT * FROM krav__training_times WHERE TID = ".$row['id']) as $training_time): ?>
        <div class="form-group col">
            <label for="exampleFormControlSelect1">Hét napja</label>
            <select class="form-control" id="exampleFormControlSelect1">
                <?php
                    foreach ($CNF->days as $key => $day) {
						$selected = ($key == $training_time['weekday']) ? 'selected' : '';
                        echo '<option value="'.$key.'" '.$selected.'>'.$day.'</option>';
                    }
                ?>
            </select>
        </div>
        <div class="form-group col">
            <label for="start">Kezdés</label> 
            <input id="start" name="start" type="time" class="form-control" value="<?php echo (isset($training_time["start"])) ? $training_time["start"] : ""; ?>">
        </div>
        <div class="form-group col">
            <label for="start">Vége</label>
            <input id="start" name="start" type="time" class="form-control" value="<?php echo (isset($training_time["end"])) ? $training_time["end"] : ""; ?>">
        </div>
		<?php endforeach; ?>
	</div>
    <hr>
	<div class="row">
		<div class="col">
			<a class="btn btn-danger confirm btn-block" href="index.php?p=<?php echo $page->current; ?>&delete=<?php echo $id; ?>">Törlés</a>
		</div>
		<div class="col text-right">
			<input type="hidden" name="edit" value="<?php echo $id; ?>">
			<input type="hidden" name="p" value="<?php echo $page->current; ?>">
			<button class="btn btn-success btn-block" type="submit" name="action" value="0">Mentés</button>
		</div>
	</div>
</form>
<?php
} else {
?>
<div class="row">
	<h2 class="col-12 col-md">Aktuális edzések</h2>
	<div class="col-12 col-md text-right">
		<a class="btn btn-primary" href="index.php?p=<?= $page->current ?>&id=0" role="button">Új edzés megadása</a>
	</div>
</div>
<form method="GET" action="index.php">
	<div class="row">
		<?php
			foreach($page->fetch("SELECT * FROM `cuisine`") as $cuisine) {
				echo '<div class="col-12 col-md-4 my-3">';
				echo '<ul class="list-group">';
				echo '<li class="list-group-item list-group-item-light font-weight-bold">'.$cuisine['name'].'</li>';
				$category = $cuisine['id'];
				foreach($page->fetch("SELECT * FROM `menu` WHERE category = $category ORDER BY active DESC, name ASC") as $food) {
					echo '<li class="list-group-item">';
					$active = ($food['active']) ? 'checked' : '';
					echo '<input name="food[]" value="'.$food['id'].'" type="checkbox" class="mr-3" id="food-'.$food['id'].'" '.$active.'><label class="form-check-label" for="food-'.$food['id'].'">'.$food['name'].'</label>';
					echo '<a href="index.php?p='.$page->current.'&id='.$food['id'].'" class="badge badge-primary float-right py-1 mt-1">Szerk.</a>';
					echo '</li>';
				}

				echo '</ul>';
				echo '</div>'; // Col end
			}

		?>
	</div>
	<div class="row mt-5">
		<div class="col text-right">
			<input type="hidden" name="p" value="<?php echo $page->current; ?>">
			<button class="btn btn-warning btn-block" type="submit" name="action" value="edit_delete_inactive">Inaktív törlése</button>
		</div>
		<div class="col text-right">
			<input type="hidden" name="p" value="<?php echo $page->current; ?>">
			<a href="printable.php" target="_blank" class="btn btn-info btn-block">Nyomtatás</a>
		</div>
		<div class="col text-right">
			<input type="hidden" name="p" value="<?php echo $page->current; ?>">
			<button class="btn btn-success btn-block" type="submit" name="action" value="edit_active">Étlap mentése</button>
		</div>
	</div>
</form>

<?php }?>
