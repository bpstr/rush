<div class="jumbotron">
	<div class="container">
		<h1 class="display-4">Üdv újra itt!</h1>
		<p class="lead"><b><?php echo ucfirst($CNF->days[date("l")]); ?></b> <?php echo $CNF->daytimeText(); ?></p>
		<div class="row">
			<div class="col s12 m4 center-align">
				<h1 class="thin"><?php echo $page->field("SELECT count(*) FROM visitor_tracking"); ?></h1>
				<h5 class="grey-text text-darken-3">Oldallekérés</h5>
			</div>
			<div class="col s12 m4 center-align">
				<h1 class="thin"><?php echo $page->field("SELECT count(*) FROM visitors"); ?></h1>
				<h5 class="grey-text text-darken-3">Látogató</h5>
			</div>
		</div>
	</div>
</div>
