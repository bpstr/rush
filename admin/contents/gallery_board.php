<?php 
if (isset($_POST["action"]) && ctype_digit($_POST["action"])) {
	$target_gallery = $_POST["action"];
	
	foreach($page->fetch("SELECT * FROM `images` WHERE GID = $target_gallery") as $original_image) {
		$image_id = $original_image['id'];
		$update_fields = [];
		
		if ($_POST["imagetitle$image_id"] !== $original_image['name'] || !empty($_FILES["fileToUpload$image_id"]['name'])) {
			$update_fields['name'] = $conn->escape_string(trim($_POST["imagetitle$image_id"]));
			$update_fields['uri'] = $CNF->toAscii(trim($_POST["imagetitle$image_id"]));
		}
		
		if (!empty($_FILES["fileToUpload$image_id"]['name'])) {
			$update_fields["image"] = uploadImage("../images/gallery/" . basename($_FILES["fileToUpload$image_id"]["name"]), "../images/gallery/".date("y-m-d")."-".$update_fields["uri"].".jpg", "fileToUpload$image_id");
		}
		
		if (!empty($update_fields)) {
			$q = array();
			foreach($update_fields as $key => $value) {
				$q[] = "`$key` = '$value'";
			}
			$result = $conn->query("UPDATE `images` SET ".implode(", ", $q)." WHERE id = ".$image_id) or die($conn->error);
			echo '<div class="alert alert-success" role="alert"><b>'.$update_fields['name'].'</b> sikeresen frissítve!</div>';
		}
		
	}
	/*
	foreach($_POST as $key => $value) {
		if (!in_array($key, array_keys($food))) continue;
		if (in_array($key, array("vegetarian", "lactose_free", "gluten_free"))) {
			$food[$key] = 1;
		} else {
			$food[$key] = $conn->escape_string($value);
		}
	}
	$food["uri"] = $CNF->toAscii(trim($food["name"]));
	
	$food["image"] = uploadImage("../images/foods/" . basename($_FILES["fileToUpload"]["name"]), "../images/foods/".date("y-m-d")."-".$food["uri"].".jpg");
	
	if ($_POST["edit"] == 0) {
		$result = $conn->query("INSERT INTO `menu` (`id`, `name`, `uri`, `description`, `category`, `price`, `image`, `active`, `vegetarian`, `lactose_free`, `gluten_free`) VALUES (NULL, '".implode("', '", $food)."');") or die($conn->error);
		if ($result) {
			echo '<div class="alert alert-success" role="alert"><b>'.$food['name'].'</b> sikeresen hozzáadva!</div>';
		}
	} elseif(ctype_digit($_POST["edit"])) {
		$q = array();
		foreach($food as $key => $value) {
			$q[] = "`$key` = '$value'";
		} 
		
		$result = $conn->query("UPDATE `menu` SET ".implode(", ", $q)." WHERE id = ".$_POST["edit"]) or die($conn->error);
		echo '<div class="alert alert-success" role="alert"><b>'.$food['name'].'</b> sikeresen frissítve!</div>';
	}

	*/
}





	foreach($page->fetch("SELECT * FROM `galleries`") as $gallery) {
		echo '<form method="POST" action="index.php" autocomplete="off" enctype="multipart/form-data" id="weekly">';
		echo '<div class="row">';
		echo '<div class="col-12">';
		echo '<h2>'.$gallery['name'].'</h2>';
		echo '</div>'; // col end
		echo '</div>'; // row end
		
		echo '<div class="row">';
		foreach($page->fetch("SELECT * FROM `images` WHERE GID = ".$gallery['id']) as $image) {
			
			echo '<div class="col-12 col-md-4 mb-3">';
			echo '<div class="card">';
			echo '<img src="../images/gallery/cropped-'.$image['image'].'" class="card-img-top">';
			
			echo '<div class="card-body form-group">';
			echo '<label for="image-'.$image['id'].'">Kép neve</label>';
			echo '<input id="image-'.$image['id'].'" name="imagetitle'.$image['id'].'" type="text" class="form-control" value="'.$image['name'].'">';
			echo '</div>';
			
			echo '<div class="card-body form-group pt-0">';
			echo '<label for="fileToUpload'.$image['id'].'">Kép cseréje</label>';
			echo '<input type="file" name="fileToUpload'.$image['id'].'" id="fileToUpload'.$image['id'].'" accept="image/*" class="form-control-file">';
			echo '</div>';
			// echo '</div>'; // body end
			echo '</div>'; // card end
			echo '</div>'; // col end
		}
			
		
		
		echo '</div>'; // row end
		echo '<div class="row mb-5">';
		echo '<div class="col text-right">';
		echo '<input type="hidden" name="p" value="'.$page->current.'">';
		echo '<button class="btn btn-success btn-block" type="submit" name="action" value="'.$gallery['id'].'">Mentés</button>';
		echo '</div>';
		echo '</div>';
		
		
		
		echo '</form>'; // Col end
		
		echo '<hr class="my-5">';
		
	}

?>