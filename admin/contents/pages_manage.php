<?php 
$page->breadcrump(); 



if (isset($_POST["edit"])) {
	$pagedata = array("name" => "", "desc" => "", "cover" => "");
	foreach($_POST as $key => $value) {
		if (!in_array($key, array_keys($pagedata))) continue;
		$pagedata[$key] = $conn->escape_string($value);
	}
	// VALIDATE INPUT FIELDS CATEGORY NOT NULL! 
	
	if (isset($_POST["filepath"])) {
		if (is_file("../images/covers/".$_POST["filepath"])) {
			$pagedata["cover"] = $conn->escape_string($_POST["filepath"]);
		} else {
			$pagedata["cover"] = uploadImage("../images/covers/" . basename($_FILES["fileToUpload"]["name"]), "../images/covers/" . basename($_FILES["fileToUpload"]["name"]));
		}
	}
	
	// $pagedata["uri"] = $CNF->toAscii($pagedata["name"]);
	if ($_POST["edit"] == 0) {
		// $result = $conn->query("INSERT INTO `menu` (`id`, `name`, `uri`, `description`, `category`, `price`, `image`, `active`, `vegetarian`, `lactose_free`, `gluten_free`) VALUES (NULL, '".implode("', '", $pagedata)."');") or die($conn->error);
		// if ($result) {
			// echo "yep";
		// }
	} elseif(ctype_digit($_POST["edit"])) {
		$q = array();
		foreach($pagedata as $key => $value) {
			$q[] = "`$key` = '$value'";
		} 
		echo "upd";
		$result = $conn->query("UPDATE `pages` SET ".implode(", ", $q)." WHERE id = ".$_POST["edit"]) or die($conn->error);
	}
	
}



if (isset($_GET["id"])) {
	$id = (ctype_digit($_GET["id"])) ? $_GET["id"]: 0;
	$row = $page->fetch("SELECT * FROM pages WHERE id = $id", false);
?>
<div class="row">
	<h3 class="col s12 light white-text"><?php echo $row["name"]; ?> szerkesztése</h3>
</div>
<div class="row">
	<div class="col s12">
		<div class="card">
			<?php if ($row["cover"] != "") {?>
            <div class="card-image" style="max-height: 200px; overflow: hidden;">
				<img src="../images/covers/<?php echo $row["cover"]; ?>" style="max-width: 100%; margin-top: -25%;" class="preview">
            </div>
			<?php } ?>
            <div class="card-content">
				<form method="POST" action="index.php" autocomplete="off" enctype="multipart/form-data">
					<span class="card-title">Oldal adatainak szerkesztése</span>
					<div class="row">
						<div class="input-field col s12 m6">
							<input id="name" name="name" type="text" class="validate" value="<?php echo $row["name"]; ?>">
							<label for="name">Oldal neve</label>
						</div>
						<div class="input-field col s12 m6">
							<p>Az oldal neve megjelenik a lap címében, a főmenüben és a megnyitott oldalon...</p>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12 m6">
							<input id="desc" name="desc" type="text" class="validate" data-length="120" value="<?php echo $row["desc"]; ?>">
							<label for="desc">Oldal leírása</label>
						</div>
						<div class="input-field col s12 m6">
							<p>Rövid részletes leírás az oldalról. Megjelenik keresőkben, valamint megosztásoknál.</p>
						</div>
					</div>
					<span class="card-title">Oldal borítóképe</span>
					<div class="row">
						<?php 
							// $colsize = "s12 m"; 
							if (isset($row["cover"])) {
								if ($row["cover"] != "") { 
									echo '<div class="file-field input-field col s12 m4 l2">';
									echo '<a href="'.$CNF->adress.'/images/covers/'.$row["cover"].'" target="_blank">';
									echo '<img src="../images/covers/'.$row["cover"].'" style="height: 96px" class="preview">';
									echo '</a>';
									echo '</div>';
								}
							}
							$colsize = "s12 m4 l8";
						?>
						<div class="file-field input-field col s12 m4 l2">
							<span class="card-title">Feltöltött képek</span>
							<a href="#uploaded" class="waves-effect waves-light btn-large modal-trigger light-green">Kiválasztás</a>
						</div>
						<div class="file-field input-field col <?php echo $colsize; ?>">
							<span class="card-title">Kép feltöltése</span>
							<div class="btn orange darken-2">
								<span>Kiválasztás</span>
								<input type="file" name="fileToUpload" id="fileToUpload" accept="image/*">
							</div>
							<div class="file-path-wrapper">
								<input class="file-path validate" name="filepath" id="filepath" type="text">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<input type="hidden" name="edit" value="<?php echo $id; ?>">
							<input type="hidden" name="p" value="<?php echo $page->current; ?>">
							<button class="btn waves-effect waves-light right orange" type="submit" name="action" value="0">Mentés<i class="material-icons right">send</i></button>
						</div>
					</div>
				</form>
			</div>
        </div>
	</div>
</div>
<div class="row">
	<h3 class="col s12 light white-text">Cikkek ezen az oldalon</h3>
</div>
<div class="row">
	<?php 
		foreach($page->fetch("SELECT * FROM articles WHERE page = '".$row["file"]."'") as $r) {
			echo '<a href="index.php?p=pages_articles&id='.$r["id"].'" class="col s12 m3">';
			echo '<div class="card small">';
			echo '<div class="card-content blue-grey-text text-darken-3">';
			if ($r['icon'] == '') $r["icon"] = "&#xE14C;";
			if ($r['title'] == '') $r["title"] = "Nincs címe";
			echo '<div class="center"><i class="material-icons medium orange-text">'.$r["icon"].'</i></div>';
			echo '<span class="card-title truncate">'.$r["title"].'</span>';
			echo '<p class="">'.$r["content"].'</p>';
			echo '</div>';
			echo '</div>';
			echo '</a>';
		}
	?>
</div>

<div id="uploaded" class="modal">
    <div class="modal-content row">
		<h4 class="col s12">Feltöltött borítóképek</h4>
		<?php 
			$used = $page->fetch("SELECT cover FROM pages WHERE cover != ''");
			$images = scandir("../images/covers");
			array_shift($images);
			array_shift($images);
			foreach($images as $img) {
				$style = '';
				if (in_array($img, array_column($used, 'cover'))) $style = "opacity: 0.6; ";
				echo '<div class="col s12 m4 l3" style="overflow: hidden;">';
				echo '<img src="../images/covers/'.$img.'" style="height: 100px; '.$style.'" onclick="$(\'#filepath\').val(\''.$img.'\'); $(\'.preview\').attr(\'src\', \'../images/covers/'.$img.'\'); $(\'#uploaded\').modal(\'close\');">';
				echo '</div>';
			} 
		?>
	
	</div>
    <div class="modal-footer">
      <a href="#" class="modal-action modal-close waves-effect waves-green btn-flat">Bezárás</a>
    </div>
  </div>

<?php
} else {
?>
<div class="row">
	<h3 class="col s12 light white-text"><?php echo $page->name; ?></h3>
</div>
<div class="row">
	<div class="col s12 m6">
		<ul class="collection with-header">
			<li class="collection-header"><h4>Publikus oldalak</h4></li>
			<?php 
				foreach($page->fetch("SELECT * FROM pages WHERE id BETWEEN 100 AND 400") as $row) {
					echo '<a href="index.php?p=pages_manage&id='.$row["id"].'" class="collection-item avatar blue-grey-text">';
					echo '<i class="material-icons circle blue-grey lighten-5 blue-grey-text">'.$row["icon"].'</i>';
					echo '<span class="title">'.$row["name"].'</span>';
					echo '<p class="truncate">'.$row["desc"].'</p>';
					echo '</a>';
				}
			?>			
		</ul>
	</div>
</div>
<?php } ?>