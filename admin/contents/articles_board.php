<?php


if (isset($_POST["edit"])) {

	$content = array("title" => "", "subtitle" => "", "content" => "");
	foreach($_POST as $key => $value) {
		if (!in_array($key, array_keys($content))) continue;

		$content[$key] = $conn->escape_string($value);
	}

	$target = $conn->escape_string($_POST['images']);

	foreach($page->fetch("SELECT * FROM `krav__article_images` WHERE article = '$target'") as $original_image) {
		$image_id = $original_image['id'];
		$update_fields = [];
		$uri = $CNF->toAscii(trim($_POST["imagetitle$image_id"]));

		if ($_POST["imagetitle$image_id"] !== $original_image['alt'] || !empty($_FILES["fileToUpload$image_id"]['name'])) {
			$update_fields['alt'] = $conn->escape_string(trim($_POST["imagetitle$image_id"]));
		}


		if (!empty($_FILES["fileToUpload$image_id"]['name'])) {
			if (is_file("../images/articles/".$original_image['src'])) rename("../images/articles/".$original_image['src'], "../trash/original-".time().$original_image['src']);
			if (is_file("../images/articles/cropped-".$original_image['src'])) rename("../images/articles/cropped-".$original_image['src'], "../trash/cropped-".time().$original_image['src']);
			if (is_file("../images/articles/scaled-".$original_image['src'])) rename("../images/articles/scaled-".$original_image['src'], "../trash/scaled-".time().$original_image['src']);
			$update_fields["src"] = uploadImage("../images/articles/" . basename($_FILES["fileToUpload$image_id"]["name"]), "images/articles/".date("y-m-d")."-$uri.jpg", "fileToUpload$image_id");
		}

		if (!empty($update_fields)) {
			$q = array();
			foreach($update_fields as $key => $value) {
				$q[] = "`$key` = '$value'";
			}
			$result = $conn->query("UPDATE `krav__article_images` SET ".implode(", ", $q)." WHERE id = ".$image_id) or die($conn->error);
			echo '<div class="alert alert-success" role="alert"><b>'.$update_fields['alt'].'</b> nevű kép feltöltve.</div>';


		}

	}

	if(ctype_digit($_POST["edit"])) {
		$q = array();
		foreach($content as $key => $value) {

			$q[] = "`$key` = '$value'";
		}
		$result = $conn->query("UPDATE `krav__articles` SET ".implode(", ", $q)." WHERE id = ".$_POST["edit"]) or die($conn->error);
		echo '<div class="alert alert-success" role="alert"><b>'.$content['title'].'</b> sikeresen frissítve!</div>';
	}
	else {
		echo '<div class="alert alert-warning" role="alert">Valami nincs rendben...</div>';
	}

}

if (isset($_GET["id"])) {
	$id = (ctype_digit($_GET["id"])) ? $_GET["id"] : 0;
	if ($id != 0) {
		$article = $page->fetch("SELECT * FROM krav__articles WHERE id = $id", false);
	} else {
		$row = array();
	}


	echo '<form method="POST" action="index.php" autocomplete="off" enctype="multipart/form-data" >';
	echo '<div class="row">';
	echo '<div class="col-12">';

	echo '<div class="row">';
	echo '<div class="form-group col-12 col-md">';
	echo '<label for="title' . $article["id"] . '">Cikk címe <code>' . $article["uri"] . '</code></label>';
	echo '<input id="title' . $article["id"] . '" type="text" name="title" class="form-control" value="' . $article["title"] . '">';
	echo '</div>';
	echo '<div class="form-group col-12 col-md">';
	echo '<label for="subtitle' . $article["id"] . '">Alcím</label>';
	echo '<input id="subtitle' . $article["id"] . '" type="text" name="subtitle" class="form-control" value="' . $article["subtitle"] . '">';
	echo '</div>';
	echo '</div>';

	echo '<div class="row">';
	echo '<div class="form-group col">';
	echo '<label for="content' . $article["id"] . '">Cikk tartalma <code>' . $article["length"] . '</code></label>';
	echo '<textarea id="content' . $article["id"] . '" name="content" class="form-control" rows="' . round($article["length"] / 32) . '">' . $article["content"] . '</textarea>';
	echo '</div>';
	echo '</div>';

	echo '</div>'; // col12 end
	echo '<hr class="my-5">';

	foreach($page->fetch("SELECT * FROM `krav__article_images` WHERE article = '".$article['uri']."'") as $image) {
		echo '<div class="col">';
		echo '<div class="card">';
		echo '<img src="../images/articles/cropped-'.$image['src'].'" class="card-img-top">';

		echo '<div class="card-body form-group">';
		echo '<label for="image-'.$image['id'].'">Kép neve</label>';
		echo '<input id="image-'.$image['id'].'" name="imagetitle'.$image['id'].'" type="text" class="form-control" value="'.$image['alt'].'">';
		echo '</div>';

		echo '<div class="card-body form-group pt-0">';
		echo '<label for="fileToUpload'.$image['id'].'">Kép cseréje</label>';
		echo '<input type="file" name="fileToUpload'.$image['id'].'" id="fileToUpload'.$image['id'].'" accept="image/*" class="form-control-file">';
		echo '</div>';
		// echo '</div>'; // body end
		echo '</div>'; // card end
		echo '</div>'; // col end
	}


	echo '</div>'; // row end
	echo '<div class="row my-5">';
	echo '<div class="col text-right">';
	echo '<input type="hidden" name="images" value="' . $article['uri'] . '">';
	echo '<input type="hidden" name="edit" value="' . $article['id'] . '">';
	echo '<input type="hidden" name="p" value="' . $page->current . '">';
	echo '<button class="btn btn-success btn-block" type="submit" name="action" value="0">Mentés</button>';
	echo '</div>';
	echo '</div>';


	echo '</form>'; // Col end

	echo '<hr class="my-5">';


} else {
	$pages_data = $page->fetch("SELECT * FROM `krav__pages` WHERE id > 99");
	$pages_data[] = ['file' => 'global', 'name' => 'Globális cikkek'];
	foreach ($pages_data as $page_data) {
		$articles = $page->fetch("SELECT * FROM `krav__articles` WHERE page LIKE '".$page_data['file']."'");
		if (count($articles) == 0) continue;
		echo '<div class="row">';
		echo '<div class="col-12">';
		echo '<h2>'.$page_data['name'].'</h2>';
		echo '</div>'; // col end
		echo '</div>'; // row end
		echo '<div class="row">';
		echo '<div class="col">';

		echo '<div class="list-group my-3">';
		foreach ($articles as $article) {
			echo '<a href="index.php?p='.$page->current.'&id='.$article['id'].'" class="list-group-item list-group-item-action">';
			echo '<div class="d-flex w-100 justify-content-between">';
			echo '<h5 class="mb-1">'.$article['title'].'</h5>';
			echo '<small>'.$CNF->ago($article['date']).'</small>';
			echo '</div>';
			echo '<small>'.$article['subtitle'].'</small>';
			echo '<p class="mb-1">'.$article['content'].'</p>';
			echo '</a>';
		}

		echo '  </div>';
		echo '</div>'; // Col end
		echo '</div>'; // row end
	}


}
