<?php
class PageManager {

	public $current;
	public $place;
	public $name;
	public $desc; // Meta
	public $description; // Plain desc
	public $cover;
	public $info;
	public $levels;
	public $data;

	public $texts;
	public $textGroups;

	public $settings;
	public $settingReplace;
	public $settingGroups;

	public $menu;

	var $errcode;

	public $fetchedRows;

	/**
	 * @var mysqli
	 */
	private $con;

	function __construct ($_mysqli, $current, $info=NULL) {
		$this->con = $_mysqli;
		$this->levels = ($current[0] == 1) ? explode('/', trim(end($current))) : array_values($_GET);
		if ($current[0] == 1) { // PAGE
			$this->place = "page";
			$field = "uri";
			$query = $this->levels[0];
		} else { // ADMIN
			$this->place = "admin";
			$field = "file";
			$query = $_GET["p"];
		}
		if (trim(end($this->levels)) == "") array_pop($this->levels);
		$result = $this->con->query("SELECT * FROM krav__pages WHERE `$field` LIKE '".$this->con->escape_string($query)."'") or die($this->con->error);
		if ($result->num_rows == 0) {
			$result = $this->con->query('SELECT * FROM krav__pages WHERE id = "404" ');
		}
		$row = $result->fetch_assoc();
		$this->current = $row["file"];
		$this->name = $row["name"];
		$this->data = $row;
		$this->cover = $row["cover"];
		$this->desc = "NemPiskóta Étkezde: ".$row["desc"];
		$this->description = $row["desc"];
		if (strlen($this->desc) < 80) {
			$this->desc .= " × Napi menü a dolgos hétköznapokra: 990 Ft × Teljes étlapunkat megtalálha a weboldalon!";
		}

		$this->loadSettings();
		var_dump($this->loadTexts());
		$this->loadMenu();
	}



	public function loadTexts ($uri = false) {
		$thispage = ($uri == false) ? $this->current : $uri;
		$texts = array();
		$groups = array();
		$result = $this->con->query("SELECT * FROM krav__articles WHERE page = '".$thispage."' OR page = 'global'");
		while($row = $result->fetch_assoc()) {
			$row["content"] = str_replace(array_keys($this->settingReplace), $this->settingReplace, $row["content"]);
			$texts[$row['uri']] = $row;
			$groups[$row['place']][] = $row;
		}
		if ($uri == false) {
			$this->texts = $texts;
			$this->textGroups = $groups;
		} else {
			return $texts; 
		}
	}

	public function loadSettings ($type = FALSE) {
		$q = ($type == FALSE) ? "" : " WHERE type LIKE '$type'";
		$settings = array();
		$groups = array();
		$replace = array();
		$result = $this->con->query("SELECT * FROM krav__settings ".$q);
		while($row = $result->fetch_assoc()) {
			$settings[$row['uri']] = $row;
			$groups[$row['type']][] = $row;
			$replace["{".$row['uri']."}"] = $row['value'];
		}
		$replace["\n\n"] = '<br>';
		$this->settings = $settings;
		$this->settingGroups = $groups;
		$this->settingReplace = $replace;
	}

	public function loadMenu () {
		$menu = array();
		// $result = $this->con->query("SELECT * FROM pages WHERE place LIKE '".$this->place."' AND permission = 0");
		$result = $this->con->query("SELECT * FROM krav__pages WHERE place LIKE '".$this->place."' AND parent = ''"); // FOR TESTING ONLY
		while($row = $result->fetch_assoc()) {
			$row["class"] = ($this->current == $row["file"]) ? "active" : "";
			$menu[] = $row;
		}
		$this->menu = $menu;
	}

	public function loadPage () {
		$reqfile = "contents/".$this->current.".php";
		if (is_file($reqfile)) return $reqfile;
		return "contents/404.php";
	}

	public function loadAnnoucement () {
		if (isset($_SESSION["annoucement"])) {
			if (is_array($_SESSION["annoucement"])) {
				var_dump($_SESSION["annoucement"]);
			} else {
				echo $_SESSION["annoucement"];
			}
		}
	}

	public function setting($uri) {
		if (in_array($uri, array_keys($this->settings))) {
			return $this->settings[$uri]["value"];
		} else {
			return "<!-- shit happened :: setting not found :: $uri -->";
		}
	}

	public function settingGroup($uri) {
		if (isset($this->settingGroups[$uri])) {
			return $this->settingGroups[$uri];
		} else {
			return "<!-- shit happened :: setting group not found :: $uri -->";
		}
	}

	public function text($uri, $plaintext = false) {
		if (in_array($uri, array_keys($this->texts))) {
			if (!isset($this->texts[$uri]["images"])) {
				$res = $this->con->query("SELECT * FROM krav__article_images WHERE article = '".$uri."'");
				$this->texts[$uri]["images"] = array();
				while ($rr = $res->fetch_assoc()) {
					$this->texts[$uri]["images"][] = $rr;
				}
			}
			return ($plaintext) ? $this->texts[$uri]["content"] : $this->texts[$uri];
		} else {
			$result = $this->con->query("SELECT * FROM krav__articles WHERE uri = '$uri'");
			if ($result->num_rows == 1) {
				$row = $result->fetch_assoc();
				$ret = array();
				$ret["title"] = $row["title"];
				$ret["content"] = str_replace(array_keys($this->settingReplace), $this->settingReplace, $row["content"]);
				$ret["uri"] = $row["uri"];
				$ret["subtitle"] = $row["subtitle"];

				$res = $this->con->query("SELECT * FROM krav__article_images WHERE article = '".$uri."'");
				$ret["images"] = array();
				while ($rr = $result->fetch_assoc()) {
					$ret["images"] = $rr;
				}
				return $ret;
			}
		}
	}

	public function textGroup($uri) {
		if (isset($this->textGroups[$uri])) {
			return $this->textGroups[$uri];
		} else {
			echo "<!-- shit happened :: article group not found :: $uri -->";
		}
	}

	public function fetch ($query, $returnArray = true) {
		$result = $this->con->query($query) or die($this->con->error);
		if (!$result) {
			echo "<!-- shit happened :: no result -->";
			return false;
		}
		$this->fetchedRows = $result->num_rows;
		if ($returnArray == false) {
			$r = array();
			while ($rr = $result->fetch_assoc()) {
				$r[] = $rr;
			}
			return (is_array($r)) ? array_shift($r) : $r;
			// return $r[0];
		} elseif ($result->num_rows > 0) {
			$r = array();
			while ($rr = $result->fetch_assoc()) {
				$r[] = $rr;
			}
			return $r;
		} else {
			echo "<!-- shit happened :: no rows returned -->";
			return array();
		}
	}

	public function field ($query, $returnFormatted = false) {
		$result = $this->con->query($query);
		if (!$result) {
			echo "<!-- shit happened :: no result -->";
			return false;
		}
		$this->fetchedRows = $result->num_rows;
		if ($returnFormatted == true) {
			// NUMBER FORMAT
			// $r = $result->fetch_all(MYSQLI_ASSOC);
			// return (is_array($r)) ? array_shift($r) : $r;
			// return $r[0];
		} elseif ($result->num_rows > 0) {
			$r = $result->fetch_assoc();
			return end($r);
		} else {
			echo "<!-- shit happened :: no rows returned -->";
			return false;
		}
	}

	public function breadcrump ($color = "blue-grey") {
		echo '<nav class="row">';
		echo '<div class="nav-wrapper scroll-x blue-grey darken-2">';
		echo '<div class="col s12">';
		echo '<a href="index.php" class="breadcrumb">Admin</a>';
		if ($this->data["parent"] != "") echo '<a href="index.php?p='.$this->data["parent"].'" class="breadcrumb">'.$this->getdata($this->data["parent"], true).'</a>';
		echo '<a href="index.php?p='.$this->current.'" class="breadcrumb">'.$this->data["name"].'</a>';
		if (isset($_GET["id"])) {
			echo '<a href="index.php?p='.$this->current.'&id='.$_GET["id"].'" class="breadcrumb">'.(($_GET["id"] == 0) ? "Létrehozás" : "Szerkesztés").'</a>';
		}
		echo '</div>';
		echo '</div>';
		echo '</nav>';
	}

	public function subpages () {
		$result = $this->con->query("SELECT * FROM krav__pages WHERE parent LIKE '".$this->current."'");
		if ($result->num_rows == 0) return false;
		echo '<div class="row">';
		// echo '<nav class="row">';
		// echo '<div class="nav-wrapper scroll-x '.(($this->cover != "") ? $this->cover : 'blue-grey').' lighten-5">';
		echo '<ul class="tabs '.(($this->cover != "") ? $this->cover : 'blue-grey').' lighten-5 tabs-transparent">';
		while($row = $result->fetch_assoc()) {
			if (is_file("contents/".$row["file"].".php")) {
				// echo '<li class="tab"><a href="index.php?p='.$row["file"].'" class="blue-grey-text text-darken-2"><i class="material-icons left '.(($this->cover != "") ? $this->cover : 'blue-grey').'-text">'.$row["icon"].'</i>'.$row["name"].'</a></li>';
				echo '<li class="tab"><a href="index.php?p='.$row["file"].'" target="_self" class="blue-grey-text text-darken-2">'.$row["name"].'</a></li>';
			} else {
				echo '<li><a class="blue-grey-text text-darken-2"><i class="material-icons left hide">'.$row["icon"].'</i>'.$row["name"].'<span class="badge right orange blue-grey-text text-darken-4" style="font-size: 10px;margin: 22px 5px;">HAMAROSAN!</span></a></li>';
			}
		}
		echo '</ul>';
		echo '</div>';
		// echo '</nav>';
	}

	public function getdata ($uri = false, $name = false) {
		if (!$uri) $uri = $this->current;
		$result = $this->con->query("SELECT * FROM krav__pages WHERE file LIKE '$uri'"); // FOR TESTING ONLY
		if ($result->num_rows > 0) {
			if ($name) {
				$r = $result->fetch_assoc();
				return $r["name"];
			} else {
				return $result->fetch_assoc();
			}
		} else {
			echo "<!-- shit happened :: article group not found :: $uri -->";
		}
	}

	public function cover ($height = false) {
		$pixels = ($height) ? 400 : 300;
		if ($_SESSION["isMobile"]) $pixels =  ($height) ? 400 : 200;
		if (is_file('images/covers/'.$this->data["cover"])) {
			echo '<div class="parallax-container page-banner" style="height: '.$pixels.'px;">';
			echo '<div class="">';
			echo '<div class="container">';
			echo '<div class="row">';
			echo '<h1 class="header col s12">'.$this->data["name"].'</h1>';
			if ($this->data["desc"] != "") echo '<p class="bold flow-text col s12 m8">'.$this->data["desc"].'</p>';
			echo '</div>';
			echo '</div>';
			echo '</div>';
			echo '<div class="parallax"><img src="images/covers/'.$this->data["cover"].'" class="responsive darken" alt="'.$this->data["name"].'"></div>';
			echo '</div>';
		} else {
			echo '<!-- no page cover defined -->';
		}
	}





	public function barChart ($result, $height, $tDays = false) {
		$mysqlDays = array("Monday" => "H", "Tuesday" => "K", "Wednesday" => "Sze", "Thursday" => "Cs", "Friday" => "P", "Saturday" => "Szo", "Sunday" => "V");
		if ($result->num_rows > 0) {
			while ($row = $result->fetch_row()) {
				if ($tDays) $row[0] = $mysqlDays[$row[0]];
				$bars[$row[0]] = intval($row[1]);
			}

			echo '<div class="barchart" style="height: '.$height.'px">';
			$max = max($bars);
			$height = $height - 28;
			foreach (array_reverse($bars, true) as $label => $value) {
				$bheight = floor($value/$max*$height);
				if ($bheight == 0) $bheight = 2;
				$width = floor(1/count($bars)*100);
				echo '<div class="bar" style="width: '.$width.'%; height: '.($height+16).'px"><div style="height: '.$bheight.'px; margin-top: '.($height-$bheight).'px"><span>'.$value.'</span></div><label>'.$label.'</label></div>';
			}
			echo '</div>';
		}
	}

	public function stepper ($result, $sessionHolder) {
		while($row = $result->fetch_assoc()) {
			$step = strtoupper(str_ireplace("ORDER_", "", $row["file"]));
			if (isset($_SESSION[$sessionHolder][$step]["done"])) {
				$href = 'href="'.$row["uri"].'"';
				$color = 'hernadi-blue-text';
				$iconC = 'hernadi-blue white-text';
			} else {
				$href = '';
				$color = 'grey-text';
				$iconC = 'grey white-text';
			}

			if ($this->current == $row["file"]) {
				$title = '<span><b>'.$row["name"].'</b></span>';
				$href = 'href="'.$row["uri"].'"';
				$color = 'hernadi-blue-text';
				$iconC = 'hernadi-blue white-text';
			} else {
				$title = '<span>'.$row["name"].'</span>';
			}
			$icon = (isset($_SESSION[$sessionHolder][strtoupper(str_ireplace("order_", "", $row["file"]))]) && $this->current != $row["file"]) ? '<i class="material-icons">&#xE876;</i>' : '<i>'.$row["icon"].'</i>';
			echo '<a '.$href.' class="col s12 m4 '.$color.'"><div class="icon '.$iconC.'">'.$icon.'</div>'.$title.'<div class="line"></div></a>';
			// $before = strtoupper(str_ireplace("ORDER_", "", $row["file"]));
		}
	}

	public function showFilters ($path) {
		global $CNF;
		echo '<div class="col s12">';
		echo '<div class="row">';
		echo '<div class="col s12 m4">';
		echo '<label>Rendezés</label>';
		echo '<select class="browser-default" onchange="order($(this).val())">';
		foreach ($CNF->orderBY as $key => $data) {
			$selected = (isset($_SESSION["ORDERBY"]["`".$data[0]."`"]) && $_SESSION["ORDERBY"]["`".$data[0]."`"] == $data[1]) ? "selected" : "";
			echo '<option value="'.$data[0].':'.$data[1].'" '.$selected.'>'.$key.'</option>';
		}
		echo '</select>';
		echo '</div>';

		echo '<div class="col s12 m4">';
		echo '<label>Szűrők</label>';
		echo '<p class="truncate">';
		$checked = (isset($_SESSION["FILTERS"]["1945"])) ? 'checked="checked"' : '';
		echo '<input type="checkbox" class="filled-in" id="filled-in-box" '.$checked.' onchange="filter(1945)"/>';
		echo '<label for="filled-in-box">Csak 1945 előtt kiadott példányok</label>';
		echo '</p>';
		echo '</div>';

		echo '<div class="col s12 m4 hide-on-small-only">';
		echo '<label>Nézet</label>';
		if ($_SESSION["layout"] == 1) {
			echo '<a href="kategoria/'.$path.'/boritok" class=" wide waves-effect waves-teal btn waves-hernadi white hernadi-blue-text truncate"><i class="material-icons cyan-text text-lighten-2 left">&#xE865;</i>'.(($_SESSION["isMobile"]) ? 'Borítók': 'Borító nézet').'</a>';
		} else {
			echo '<a href="kategoria/'.$path.'/reszletes" class=" wide waves-effect waves-teal btn waves-hernadi white hernadi-blue-text truncate"><i class="material-icons cyan-text text-lighten-2 left">&#xE8E9;</i>'.(($_SESSION["isMobile"]) ? 'Részletes': 'Részletes nézet').'</a>';

		}
		echo '</div>';

		echo '</div>';
		echo '</div>';
	}

	public function printCuisine ($raw, array $classes = [], int $limit = NULL) {
		global $CNF;
		$uri = $this->con->escape_string($raw);
		$q = "SELECT * FROM krav__cuisine WHERE uri = '$uri'";
		if (!empty($limit)) $q .= " LIMIT 0, $limit";
		$cuisine = $this->fetch($q);
		$cuise = end($cuisine);
		echo '<div class="card mb-5 '.implode(' ', $classes).' overflow-hidden">';
		echo '<div class="card-img-container" style="height: 0px; padding-bottom: 50%; overflow: hidden; position: relative;">';
		echo '<img src="'.$CNF->adress
		.'images/cuisine/'.$cuise["image"].'" class="card-img-top" alt="'.$cuise["name"].'">';
		echo '<svg style="pointer-events: none" class="wave" width="100%" height="50px" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1920 75"><defs><style>.a{fill: none;}.b{clip-path: url(#a);}.c, .d{fill: #ffffff;}.d{opacity: 0.5; isolation: isolate;}</style><clipPath id="a"><rect class="a" width="1920" height="75"></rect></clipPath></defs><title>wave</title><g class="b"><path class="c" d="M1963,327H-105V65A2647.49,2647.49,0,0,1,431,19c217.7,3.5,239.6,30.8,470,36,297.3,6.7,367.5-36.2,642-28a2511.41,2511.41,0,0,1,420,48"></path></g><g class="b"><path class="d" d="M-127,404H1963V44c-140.1-28-343.3-46.7-566,22-75.5,23.3-118.5,45.9-162,64-48.6,20.2-404.7,128-784,0C355.2,97.7,341.6,78.3,235,50,86.6,10.6-41.8,6.9-127,10"></path></g><g class="b"><path class="d" d="M1979,462-155,446V106C251.8,20.2,576.6,15.9,805,30c167.4,10.3,322.3,32.9,680,56,207,13.4,378,20.3,494,24"></path></g><g class="b"><path class="d" d="M1998,484H-243V100c445.8,26.8,794.2-4.1,1035-39,141-20.4,231.1-40.1,378-45,349.6-11.6,636.7,73.8,828,150"></path></g></svg>';
		echo '</div>';
		echo '<div class="card-body">';
		echo '<h5 class="m-0">'.$cuise["name"].'</h5>';
		echo '</div>';
		echo '<ul class="list-group list-group-flush">';
			$menu[$cuise["uri"]] = $this->fetch("SELECT * FROM krav__menu WHERE category = ".$cuise["id"]);
			if ($menu[$cuise["uri"]]) {
				$n = 1;
				foreach ($menu[$cuise["uri"]] as $row) {
					echo '<li class="list-group-item d-flex justify-content-between align-items-center">';
					echo '<div><b class="d-block font-serif">'.$row["name"].'</b><span class="d-block font-italic">'.$row["description"].'</span></div>';
					echo '<span style="cursor: default; font-size: 16px" class="badge badge-primary font-serif">'.$row["price"].' Ft</span>';
					echo '</li>';
					// if ($n < count($menu[$cuise["uri"]])) echo '<div class="divider"></div>';
					$n ++;
				}
			}
		echo '</ul>';
		echo '</div>';

	}

	public function printSpecials ($todaySpecial, $speciality) {
		echo '<div class="row align-items-center">';
		echo '<div class="col-md-5 col-lg-4">';
		echo '<img class="img-fluid rounded-lg border-soft mb-3 mb-md-0 ml-md-5" src="images/daily/'.$todaySpecial["image"].'" alt="">';
		echo '</div>';
		echo '<div class="col-md-7 col-lg-8 bg-white ml-md-n5 rounded shadow-soft p-3">';
		echo '<span class="text-muted font-italic font-serif">'.$speciality["title"].'</span>';
		echo '<h2 class="font-serif font-weight-light">'.$todaySpecial["name"].'</h2>';
		echo '<p class="lead">'.$todaySpecial["description"].'</p>';
		// echo '<div class="fb-like" data-href="kulonlegesseg/'.$todaySpecial["id"].'/'.$todaySpecial["uri"].'" data-layout="standard" data-action="recommend" data-size="small" data-show-faces="false" data-share="true"></div>';
		echo '<hr class="divider my-4 mr-auto ml-1"></p>';
		echo '<p class="lead">'.$speciality["content"].'</p>';
		echo '</div>';
		echo '</div>';
	}

	public function printDailyMenu($day, $soup, $meal) {
		echo '<div class="col-md-6 col-lg-4 mb-4">';
		echo '<div class="card rounded shadow-soft meal-card mt-5">';
		echo '<div class="card-body text-center position-relative pt-5">';
		echo '<em class="text-muted font-serif display-4 position-absolute">'.$day.'</em>';
		echo '<p><span class="lead font-serif font-weight-bold">'.$soup.'</span></p>';
		echo '<hr class="divider mx-auto" />';
		echo '<p><span class="lead font-serif font-weight-bold">'.$meal.'</span></p>';
		// echo '<p class="lead font-serif font-weight-bold">'.$meal.'</p>';
		echo '</div>';
		echo '</div>';
		echo '</div>';
	}

	public function printEvent($event) {
		global $CNF;
		echo '<div class="col-sm-6">';
		$shadow = (empty($event['link'])) ? 'shadow-soft' : 'shadow';
		echo '<div class="card rounded '.$shadow.' overflow-hidden mb-5">';
		echo '<div class="row no-gutters">';

		if (!empty($event['image'])) {
			echo '<div class="col-md-4">';
			$imagelink = (strpos($event['image'], 'http') === false) ? $CNF->adress . 'images/feed/'.$event["image"] : $event["image"];
			echo '<a href="'.$imagelink.'" target="_blank">';
			echo '<img src="'.$imagelink.'" class="img-fluid" alt="'.$event["title"].'">';
			echo '</a>';
			echo '</div>';
		}

		echo '<div class="col-md-8 d-flex flex-column">';
		echo '<div class="card-body pb-1">';
		if (!empty($event['expire']) && $event['expire'] != '0000-00-00 00:00:00') {
			$expire = strtotime($event['expire']);
			echo '<span class="text-muted font-italic font-serif">'.date('Y. ', $expire).$CNF->months[date("n", $expire)].date(' d.', $expire).'</span>';
		}
		if (!empty($event['subtitle'])) {
			echo '<span class="text-muted font-italic font-serif">'.$event['subtitle'].'</span>';
		}
		echo '<h3 class="font-serif card-title">'.$event["title"].'</h3>';
		echo '<p class="card-text">'.$event["content"].'</p>';
		if (!empty($event['link'])) {
			echo '</div>'; // card-body

			echo '<div class="card-footer d-flex justify-content-between align-items-center ">';
			echo '<small class="text-muted">Közzétéve '.$CNF->ago($event['date'], 1).'</small>';
			$button = (empty($event['button'])) ? 'Részletek' : $event['button'];
			echo '<a href="'.$event['link'].'" target="_blank" class="btn btn-primary btn-sm">'.$button.'</a>';
			echo '</div>';
		} else {
			echo '<p class="card-text"><small class="text-muted">Közzétéve '.$CNF->ago($event['date'], 1).'</small></p>';
			echo '</div>'; // card-body
		}

		echo '</div>'; // col md8
		echo '</div>'; // row
		echo '</div>'; // card end
		echo '</div>'; // col end
	}

}
?>
