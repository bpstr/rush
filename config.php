<?php
class Config {

	public $ml;
	public $adress;
	public $title;

	public $host;
	public $rootdir;

	public $from;
	public $mail;

	public $bookstatus;
	public $orderstatus;
	public $deliveryIcons;
	public $orderCustomer;
	public $payment;

	public $getcwd;
	public $open;


	function __construct () {
		if (strpos($_SERVER['SERVER_NAME'], "onvedelem-kravmaga.hu") !== false) {
			$this->ml = array("localhost", "onvedelemkravma", "luc345", "onvedelemkravma");
			$this->adress = "http://www.onvedelem-kravmaga.hu/test";
		} else {
			$this->ml = array("localhost", "root", "", "onvedelem_kravmaga");
			$this->adress = "http://".$_SERVER['SERVER_NAME']."/kravmaga/";
		}

		$this->rootdir = "/var/www/clients/client257/web703/web/nempiskota.eu/";

		$this->title = "Önvédelem – Kravmaga";

		$this->open = array(
			"Hétfő" => "12-18",
			"Kedd" => "10-18",
			"Szerda" => "12-19",
			"Csütörtök" => "10-18",
			"Péntek" => "12-18",
			"Szombat" => "Zárva",
			"Vasárnap" => "Zárva"
		);

		$this->days = array(
			"Monday" => "Hétfő",
			"Tuesday" => "Kedd",
			"Wednesday" => "Szerda",
			"Thursday" => "Csütörtök",
			"Friday" => "Péntek",
			"Saturday" => "Szombat",
			"Sunday" => "Vasárnap"
		);

		$this->months = array("", "január","február","március","április","május","június","július","augusztus","szeptember","október","november","december");

		$this->settingNames = array(
			"meta" => "Meta",
			"opening" => "Nyitvatartás",
			"site" => "Weboldal",
			"contact" => "Előrhetőség",
			"social" => "Közösségi"
		);


		$this->getcwd = getcwd();

		$this->nofollow = array("cartpage", "order_data", "order_post", "order_send", "login");

	}

	// Some misc functions
	public function toAscii($str, $replace=array(), $delimiter='-') {
		if( !empty($replace) ) {
			$str = str_replace((array)$replace, ' ', $str);
		}

		$clean = preg_replace(array('/á/', '/é/', '/ö/', '/ó/', '/ő/', '/ü/', '/ű/', '/í/'), array('a', 'e', 'o', 'o', 'o', 'u', 'u', 'i'), $str);
		$clean = @iconv('UTF-8', 'ASCII//TRANSLIT', $clean);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

		return $clean;
	}

	public function ago($datetime, $length=2) {
		$now = new DateTime;
		$ago = new DateTime($datetime);
		$diff = $now->diff($ago);

		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;

		$string = array(
			'y' => 'éve',
			'm' => 'hónapja',
			'w' => 'hete',
			'd' => 'napja',
			'h' => 'órája',
			'i' => 'perce',
			's' => 'másodperce',
		);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v;
			} else {
				unset($string[$k]);
			}
		}

		$string = array_slice($string, 0, $length);
		return $string ? implode(', ', $string) . ' ' : 'Pont most';
	}

	public function daytimeText($hour = true) {
		$daytime = array( // 24h format
			3 => "késő éjszaka",
			6 => "kora hajnal",
			7 => "kora reggel",
			10 => "reggel",
			12 => "délelőtt",
			15 => "kora délután",
			17 => "este",
			21 => "éjszaka",
			24 => "késő éjszaka",
		);
		if($hour === true) $hour = date("G");
		foreach($daytime as $hr => $name) {
			if ($hr >= $hour) return $name;
		}
	}


	public function mail_utf8_old($to, $subject = '(No subject)', $message = '', $spaem = false) {
		$from_user = "=?UTF-8?B?".base64_encode($this->from)."?=";
		$subject = "=?UTF-8?B?".base64_encode($subject)."?=";

		$headers = "From: $from_user <".$this->mail.">\r\n".
			   // "Return-Path: <spaem@hernadi-antikvarium.hu>" . "\r\n" .
			   "MIME-Version: 1.0" . "\r\n" .
			   "Content-type: text/html; charset=UTF-8" . "\r\n";

		if (!$spaem) mail("hernadi.antikvarium@gmail.com", $subject, $message, $headers);
		return mail($to, $subject, $message, $headers);
   }

   public function mail_compose ($sample, $CID, $WID = 0) {
		$cdir = getcwd();
		chdir($this->rootdir);
		if (!is_file("includes/mailing/templates/".$sample.".php") || $CID < 1) {
			chdir($cdir);
			return false;
		}
		$w = $WID;
		$r = $CID;
		ob_start();
		include "includes/mailing/templates/".$sample.".php";
		$mail = ob_get_clean();

		chdir($cdir);

		if ($mail == false || $mail == "") return false;
		return $mail;
   }

	public function mail_utf8 ($target, $subject = '(No subject)', $message = '', $spaem = false) {
		global $mailer;
		global $conn;
		preg_match('/<(.*?)>/', $target, $match);
		$to = array($match[1], trim(str_replace('<'.$match[1].'>', "", $target)));
		if ($spaem) var_dump($to);
		$mailer->From     = 'noreply@nempiskota.eu';            // Felado e-mail cime
		$mailer->FromName = 'NemPiskóta Étkezde';                // Felado neve
		$mailer->addAddress($to[0], $to[1]);         			// Cimzett es neve
		// if (!$spaem) $mailer->AddBCC('info@nempiskota.eu', "NemPiskóta Étkezde");       // Meg egy cimzett
		$mailer->AddReplyTo('info@nempiskota.eu', 'NemPiskóta Étkezde'); // Valaszlevel ide

		$mailer->IsHTML(true);                                       // Kuldes HTML-kent

		// $message = chunk_split(base64_encode($message));
		// $mail->Encoding = 'base64'; // CAUSED SPAMASSASIN ERROR


		// $mailer->DKIM_domain = 'hernadi-antikvarium.hu';
		// $mailer->DKIM_private = 'signit.dkim';
		// $mailer->DKIM_selector = '1507161577.antikvarium';
		// $mailer->DKIM_passphrase = '';
		// $mailer->DKIM_identity = $mailer->From;

		$mailer->Subject = $subject;					// A level targya
		$mailer->Body    = $message;          			// A level tartalma
		$mailer->AltBody = preg_replace('/\s{3}/', '', strip_tags($message));     	// A level szoveges tartalma
		if (!$mailer->Send()) {
			// $conn->query("INSERT INTO `events` (`id`, `date`, `type`, `value`) VALUES (NULL, '".date("Y-m-d H:i:s")."', 'EMAIL_FAIL', 'Email was not sent to '".$to[0]."' (Subject: $subject) :: ".$conn->escape_string($mailer->ErrorInfo)." );");
			$mailer->ClearAllRecipients();
			$mailer->ClearAddresses();
			return false;
		} else {
			$MAIL = md5(date("YW")."-".$match[1]);
			$res = $conn->query("INSERT INTO `sent_mails` (`id`, `hash`, `week`, `subscriber`, `date`, `title`, `content`, `ipaddress`, `date_opened`) VALUES (NULL, '$MAIL', '".date("W")."', '".$match[1]."', CURRENT_TIMESTAMP, '".$conn->escape_string($subject)."', '".$conn->escape_string($mailer->AltBody)."', NULL, NULL);");
			$conn->query("UPDATE `newsletter` SET `sent` = `sent` + 1, `lastmail` = CURRENT_TIMESTAMP WHERE `email` LIKE '".$match[1]."'; ");
		}
		$mailer->ClearAllRecipients();
		$mailer->ClearAddresses();
		return true;
	}

   public function openingHours($showWeekend = true) {
	   echo '<table class="striped">';
	   echo '<tr><td>Hétfő</td><td><b>'.$this->open["Hétfő"].'</b> óra között</td></tr>
					<tr><td>Kedd</td><td><b>'.$this->open["Kedd"].'</b> óra között</td></tr>
					<tr><td>Szerda</td><td><b>'.$this->open["Szerda"].'</b> óra között</td></tr>
					<tr><td>Csütörtök</td><td><b>'.$this->open["Csütörtök"].'</b> óra között</td></tr>
					<tr><td>Péntek</td><td><b>'.$this->open["Péntek"].'</b> óra között</td></tr>';
		if ($showWeekend) echo '<tr><td>Szombat</td><td>'.$this->open["Szombat"].'</td></tr>
					<tr><td>Vasárnap</td><td>'.$this->open["Vasárnap"].'</td></tr>';
		echo '</table>';
   }

	public function dateMonth ($date = "today") {
		$time = strtotime($date);
		$months = array ("", "január","február","március","április", "május","június","július","augusztus", "szeptember","október","november","december");
		return $months[date("n", $time)];
	}

	public function wordCrop ($string, $len = 140) {
		if (strlen($string) < $len) return $string;
		$str = wordwrap($string, $len, "<br*/>");
		$str = explode("<br*/>", $str);
		return (isset($str[0])) ? $str[0]."..." : false;
	}

	public function saleLayout ($imageFilename) {
		$saleLayoutHelper = array(
			"align" => array("r" => "right-align", "l" => "left-align", "c" => "center-align darker"),
			"text" => array("l" => "white-text", "d" => "black-text")
		);
		if (!strpos($imageFilename, "_")) return array("align" => "right-align", "text" => "white-text");
		$imageTextColor = substr($imageFilename, 0,1);
		$imageAlign = substr($imageFilename, 1,1);
		$text = (in_array($imageTextColor, array_keys($saleLayoutHelper["text"]))) ? $saleLayoutHelper["text"][$imageTextColor] : "white-text";
		$align = (in_array($imageAlign, array_keys($saleLayoutHelper["align"]))) ? $saleLayoutHelper["align"][$imageAlign] : "right-align";
		return array("align" => $align, "text" => $text);
	}

	public function minifyHTML ($inputHTML) {
		$search = array(
			'/\>[^\S ]+/s',     // strip whitespaces after tags, except space
			'/[^\S ]+\</s',     // strip whitespaces before tags, except space
			'/(\s)+/s',         // shorten multiple whitespace sequences
			'/<!--(.|\s)*?-->/' // Remove HTML comments
		);

		$replace = array(
			'>',
			'<',
			'\\1',
			''
		);

		$inputHTML = preg_replace($search, $replace, $inputHTML);

		return $inputHTML;
	}

	public function client_ip() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';

		return $ipaddress;
	}


	public function randomPassword($len = 6) {
		$alphabet = 'ABCDEFGHJKLMNPRSTUVWXY123456789';
		// $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < $len; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}

	public function notify ($message, $icon = 4) {
		$ch = curl_init();
		$data = array(
			'k' => "GsQMUexylKs6gcbyxsZS",
			't' => urldecode($this->title),
			'm' => urldecode($message),
			'd' => "8624"
		);
		if (ctype_digit($icon)) {
			$data["i"] = $icon;
		} elseif (is_file($icon)) {
			$path = $icon;
			$type = pathinfo($path, PATHINFO_EXTENSION);
			$image = file_get_contents($path);
			$data["p"] = 'data:image/' . $type . ';base64,' . base64_encode($image);
		} else {
			echo "no icon defined";
		}
		$postString = http_build_query($data, '', '&');
		curl_setopt($ch, CURLOPT_URL, 'https://www.pushsafer.com/api' );
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false );
		$response = curl_exec($ch);
		curl_close($ch);
		$res = json_decode($response);
		if (isset($res->status) && $res->status == 1) return true;
	}

	public function fetch ($query, $returnArray = true) {
		$result = $this->con->query($query) or die($this->con->error);
		if (!$result) {
			echo "<!-- shit happened :: no result -->";
			return false;
		}
		$this->fetchedRows = $result->num_rows;
		if ($returnArray == false) {
			$r = $result->fetch_all(MYSQLI_ASSOC);
			return (is_array($r)) ? array_shift($r) : $r;
			// return $r[0];
		} elseif ($result->num_rows > 0) {
			return $result->fetch_all(MYSQLI_ASSOC);
		} else {
			echo "<!-- shit happened :: no rows returned -->";
			return array();
		}
	}

	public function field ($query, $returnFormatted = false) {
		$result = $this->con->query($query);
		if (!$result) {
			echo "<!-- shit happened :: no result -->";
			return false;
		}
		$this->fetchedRows = $result->num_rows;
		if ($returnFormatted == true) {
			// NUMBER FORMAT
			// $r = $result->fetch_all(MYSQLI_ASSOC);
			// return (is_array($r)) ? array_shift($r) : $r;
			// return $r[0];
		} elseif ($result->num_rows > 0) {
			$r = $result->fetch_assoc();
			return end($r);
		} else {
			echo "<!-- shit happened :: no rows returned -->";
			return false;
		}
	}

}?>
