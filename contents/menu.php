	<!-- Masthead -->
	<?php $article = $page->text("menu");?>
	<header class="" style="background-image: url(http://nempiskota.eu/next/images/samples/andy-chilton-56332-unsplash.jpg); background-size: cover;">
		<div class="container py-5">
			<div class="row"> 
				<div class="col-md-8 col-lg-6 offset-md-2 offset-lg-3 text-center intro-heading">
					<h2 class="font-serif display-3 my-3"><?= $article['title']; ?></h2>
					<hr class="divider my-4 mx-auto ml-3">
					<p class="lead"><?= $page->description; ?></p>
				</div>
			</div>
		</div>
	</header>
	
	<!-- Menu -->
	<section>
		<div class="container mt-n5">
			<div class="bg-default rounded" id="etlap">
			
				<?php 
					$cuisine = $page->fetch("SELECT * FROM cuisine");
					echo '<ul class="nav nav-pills justify-content-center py-3">';
					foreach ($cuisine as $row) {
						$active = '';
						if (isset($page->levels[1])) $active = ($row["uri"] == $page->levels[1]) ? ' active' : '';
						// echo '<li class="nav-item"><a onclick="document.title = \''.$row["name"].'\';" class="nav-link '.$active.'"  href="#'.$row["uri"].'">'.$row["name"].'</a></li>';
					}
					echo '</ul>';
					
					echo '<div class="row">';
					echo '<div class="col-sm-12 col-md-4">';
					
					$page->printCuisine('reggeli-ajanlatunk', ['shadow-lg', 'rounded']);
					
					
					$page->printCuisine('tesztak', ['rounded']);
					
					$page->printCuisine('salatak', ['rounded']);
					echo '</div>';
					
					
					echo '<div class="col-sm-12 col-md-4">';
						$page->printCuisine('levesek', ['rounded']);
						$page->printCuisine('feltetek', ['rounded']);
						$page->printCuisine('desszertek', ['shadow-lg', 'rounded']);
					
					echo '</div>';
					
					echo '<div class="col-sm-12 col-md-4">';
						$page->printCuisine('foetelek', ['shadow', 'rounded']);
						$page->printCuisine('koretek', ['rounded']);
					
					echo '</div>';
					
					echo '</div>'; // Row end
					
				?>
			</div>
		</div>
	</section>
<!--
	TODO:
	 on ACTIVE tab and tab ONCLICK async load tiny images 
	 -> menu item dialog loads large image :) image materialboxed 
--> 
	<hr/>
	
	
	<section class="my-5" id="kiszallitas">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-5 col-lg-3">
					<img src="http://nempiskota.eu/images/food_delivery.jpg" class="mw-100 rounded shadow">
				</div> 
				<div class="col-sm-12 col-md-7 col-lg-9">
					<div class="py-5">
						<?php  
							$order = $page->text("rendeles-hazhozszallitas");
							echo '<h1 class="thin font-serif">'.$order["title"].'</h1>';
							echo '<p class="lead">'.$order["content"].'</p>';
							echo '<p class="lead font-serif">Rendelés: <span class="font-weight-bold">'.$page->setting('phone').'</span></p>';
						?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<hr/>

	
	<section class="my-5">
		<div class="container">
		<?php 
			$specs = $page->fetch("SELECT * FROM daily_offer ORDER BY date DESC LIMIT 0, 16");
			if (!empty($specs)) {
				echo '<div class="row my-5">';
				echo '<div class="col-md-8 col-lg-6 offset-md-2 offset-lg-3 text-center intro-heading">';
				echo '<div class="icon"><i class="material-icons md-48 md-dark">local_dining</i></div>';
				echo '<h2 class="font-serif h1 my-3">Eddigi napi specialitásaink</h2>';
				echo '<p>Mindennap változó szezonális ételek, éppen amit a piacon kapunk és friss:)</p>'; // todo
				echo '</div>';
				echo '</div>';
				
				echo '<div class="card-columns py-5">';
				
				foreach($specs as $row) {
					echo '<a class="card rounded text-decoration-none" href="kulonlegesseg/'.$row["id"].'/'.$row["uri"].'">';
					echo '<img src="https://nempiskota.eu/images/daily/'.$row["image"].'" alt="NemPiskóta Étkezde: '.$row["name"].'" class="card-image-top mw-100">';
					echo '<div class="card-body">';
					echo '<small class="text-muted font-italic font-serif">'.ucfirst($CNF->months[date("n", strtotime($row["date"]))]).' '.date("j", strtotime($row["date"])).'. '.$CNF->days[date("l", strtotime($row["date"]))].'</small>';
					echo '<p class="card-title text-black">'.$row["name"].'</p>';
					echo '<p class="card-text">'.$row["description"].'</p>';
					echo '</div>';
					echo '</a>';
				}
				echo '</div>';
			}
		?>
	</section>