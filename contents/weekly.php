	<section class="my-5">
		<div class="container">
		<?php 
			$future = $page->fetch("SELECT * FROM weekly_offer WHERE date_from >= '".date("Y-m-d H:i:s")."' ORDER BY date_from DESC");
			if (!empty($future)) {
				echo '<div class="row my-5">';
				echo '<div class="col-md-8 col-lg-6 offset-md-2 offset-lg-3 text-center intro-heading">';
				echo '<div class="icon"><i class="material-icons md-48 md-dark">local_dining</i></div>';
				echo '<h2 class="font-serif h1 my-3">Következő napi menü ajánlataink</h2>';
				echo '<p>Az elkövetkező hetekben az alábbi napi ajánlatainnkkal várunk Titeket!.</p>';
				echo '</div>';
				echo '</div>';
				
				foreach ($future as $week) {
					
					
					echo '<div class="col-md-6 col-lg-4 mb-4">';
					echo '<div class="card rounded shadow-soft bg-primary ">';
					echo '<div class="card-body align-items-center justify-content-center text-center text-white">';
					echo '<p class="card-title">Érvényes:</p>';
					echo '<p class="h2 font-serif">'.date('Y. m. d.', strtotime($week["date_from"])).'</p>';
					echo '<p class="h2 font-serif">'.date('Y. m. d.', strtotime($week["date_to"])).'</p>';
					echo '</div>';
					echo '</div>';
					echo '</div>';
					
					foreach(array_slice($CNF->days, 0, 5) as $day => $translated) {
						$page->printDailyMenu($translated, $week[$day."_soup"], $week[$day."_meal"]);
					}
				}
				
			}
			
		?>
		</div>
	</section>
	
	
	<?php $article = $page->text("weekly"); ?>
	<section class="my-5">
		<div class="container">
			<div class="row my-5">
				<div class="col-md-8 col-lg-6 offset-md-2 offset-lg-3 text-center intro-heading">
					<div class="icon"><i class="material-icons md-48 md-dark">local_dining</i></div>
					<h2 class="font-serif display-3 my-3"><?= $article['title']; ?></h2>
					<p class="lead"><?= strip_tags($article['content']); ?><br>Érvényes: <?= date( 'Y. m. d.', strtotime( 'monday this week' ) ); ?> - <?= date( 'Y. m. d.', strtotime( 'sunday this week' ) ); ?></p>
				</div>
			</div>
			<div class="row">
				<?php 
					$week = $page->fetch("SELECT * FROM weekly_offer WHERE week = ".date("W")." ORDER BY date_from DESC", false);
					foreach(array_slice($CNF->days, 0, 5) as $day => $translated) {
						$page->printDailyMenu($translated, $week[$day."_soup"], $week[$day."_meal"]);
					}
					
					echo '<div class="col-md-6 col-lg-4 mb-4">';
					echo '<div class="card rounded shadow-soft bg-primary meal-card mt-4">';
					echo '<div class="card-body text-white">';
					echo '<span class="font-serif h2 font-italic">Minden nap frissen készült, laktató ebédmenü!</span>';
					echo '<p class="display-4 font-serif text-right">1090 Ft</p>';
					echo '</div>';
					echo '</div>';
					echo '</div>';
				?>
				
			</div>
		</div>
	</section>
	
	<section class="my-5">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-4 col-lg-3">
					<img src="https://nempiskota.eu/images/food_delivery.jpg" class="mw-100 rounded shadow-soft">
				</div> 
				<div class="col-sm-12 col-md-8 col-lg-9">
					<div class="py-5">
						<?php  
							$order = $page->text("rendeles-hazhozszallitas");
							echo '<h1 class="thin font-serif">'.$order["title"].'</h1>';
							echo '<p class="lead">'.$order["content"].'</p>';
							echo '<p class="lead font-serif">Rendelés: <span class="font-weight-bold">'.$page->setting('phone').'</span></p>';
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="my-5">
		
		<?php 
			$shownSpecials = array();
		
			$past = $page->fetch("SELECT * FROM weekly_offer WHERE date_to < '".date("Y-m-d H:i:s")."' ORDER BY date_from DESC LIMIT 0, 10");
			if (!empty($past)) {
				echo '<div class="container">';
				echo '<div class="row my-5">';
				echo '<div class="col-md-8 col-lg-6 offset-md-2 offset-lg-3 text-center intro-heading">';
				echo '<div class="icon"><i class="material-icons md-48 md-dark">local_dining</i></div>';
				echo '<h2 class="font-serif h1 my-3">Korábbi napi ajánlataink</h2>';
				echo '<p>Minden héten új napi menü ajánlattal készülünk Nektek! <br>Az elmúlt néhány hét ajánlatait láthatjátok.</p>';
				echo '</div>';
				echo '</div>';
				echo '</div>'; // Container end
				
				foreach ($past as $week) {
					echo '<div class="container">';
					echo '<div class="row my-5">';
					
					echo '<div class="col-md-6 col-lg-4 mb-4">';
					echo '<div class="card meal-card rounded bg-white pt-4 pb-1 mt-5">';
					echo '<div class="card-body align-items-center justify-content-center text-center text-muted">';
					echo '<em class="text-primary font-serif display-4 position-absolute">'.$week["week"].'. hét</em>';
					echo '<div class="h2 font-serif">'.date('Y. m. d.', strtotime($week["date_from"])).'</div>';
					echo '<hr class="divider mx-auto my-4" />';
					echo '<div class="h2 font-serif">'.date('Y. m. d.', strtotime($week["date_to"])).'</div>';
					echo '</div>';
					echo '</div>';
					echo '</div>';
					
					foreach(array_slice($CNF->days, 0, 5) as $day => $translated) {
						echo '<div class="col-md-6 col-lg-4 mb-4">';
						echo '<div class="card meal-card rounded shadow-soft">';
						echo '<div class="card-body text-center">';
						echo '<div class="card-header text-center bg-white mb-4 pt-0"><small class="text-muted mb-5">'.$translated.'</small></div>';
						echo '<p><span class="lead font-serif font-weight-bold">'.$week[$day."_soup"].'</span></p>';
						echo '<hr class="divider mx-auto" />';
						echo '<p><span class="lead font-serif font-weight-bold">'.$week[$day."_meal"].'</span></p>'; 
						echo '</div>';
						echo '</div>';
						echo '</div>';
					}
					
					echo '</div>'; // Row end
					$q = "SELECT * FROM daily_offer WHERE `date` BETWEEN '".$week["date_from"]."' AND '".$week["date_to"]."'";
					if (!empty($shownSpecials)) $q .= " AND id NOT IN (".implode(',', $shownSpecials).")";
					$q .= " ORDER BY `date` DESC";
					foreach($page->fetch($q) as $special) {
						$text = array(
							'title' => 'Napi specialitásunk:',
							'content' => 'Dátum: '.date('Y. m. d.', strtotime($special["date"]))
						);
						$page->printSpecials($special, $text);
						$shownSpecials[] = $special['id'];
					}
					
					echo '</div>'; // Container end
					echo '<hr />';
				}
				
			}

		?>
	</section>
