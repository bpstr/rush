<?php $article = $page->text("trainer");?>
<section class="mt-5 overflow-hidden">
	<div class="container-fluid pt-5">
        <div class="row my-5">
			<div class="col-xl-5 col-lg-8 offset-xl-1"><h1 class="display-3 font-weight-bold">Óber Tamás</h1><h3 class="text-primary ml-3"><?= $article['subtitle']; ?></h3><p class="lead mt-5"><?= $article['content']; ?></p></div>
			<div class="col-xl-4 col-lg-4 offset-xl-1 col-md-8 gold-shadow"><img src="images/articles/scaled-<?= $article['images'][0]['src']; ?>" class="w-100 gold-shadow"></div>
		</div>
		<div class="row my-5">
			<?php $articles = $page->textGroup("diploma"); ?>
			<div class="col-xl-2 col-lg-3 offset-xl-1 col-md-3 col-sm-6"><img src="images/articles/scaled-<?= $articles[0]['images'][0]['src']; ?>" class="w-100 bg-primary pb-3"></div>
			<div class="col-xl-2 col-lg-3 col-md-3 col-sm-6 m"><img src="images/articles/scaled-<?= $articles[1]['images'][0]['src']; ?>" class="w-100 bg-primary p-3 pb-5"></div>
			<div class="col-xl-6 col-lg-10 offset-xxl-1 offset-xl-0 offset-md-1 offset-lg-2"><h1 class="display-3 font-weight-bold">Diplomák</h1>
				<?php 
					foreach ($articles as $article) {
						echo '<h4 class="mt-5 ">'.$article['title'].'</h4>';
						echo '<p class="lead m-3">'.$article['content'].'</p>';
					}
				?>
			</div>
		</div>
    </div> 
</section> 
