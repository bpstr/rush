<?php 


if (isset ($_POST["email"])) {
	$mail = $conn->escape_string($_POST["email"]);

	if ($_POST["email"] == "") $ERR["email"] = "Kérjük adja meg e-mail címét!";
	if ($_POST["email"] != $mail) $ERR["email"] = "Kérjük valós e-mail címet adjon meg!";
	$result = $conn->query("SELECT * FROM newsletter WHERE email LIKE '$mail'");
	$conn->query("UPDATE newsletter SET `is_allowed` = 0 WHERE email LIKE '$mail'");
	echo '<div class="row"><h1>Sikeres leiratkozás!</h1></div>';
	echo '<script>window.location.href = \'\';</script>';
} 

if (isset($page->levels[1]) && $page->levels[1] == "feliratkozas") { // SUBSCRIBTIONS
	$ERR = array();
	// Validate sent subscription 
	if (isset($_POST["name"]) && isset($_POST["email"])) {
		if (strlen($_POST["name"]) < 3) $ERR["name"] = "Adjon meg valós nevet!";
		if (strlen($_POST["email"]) < 3) $ERR["email"] = "Töltse ki az e-mail címet!";
		if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) $ERR["email"] = "Valós e-mail címet adjon meg!";
		$name = $conn->escape_string($_POST["name"]);
		$email = $conn->escape_string($_POST["email"]);
		$result = $conn->query("SELECT * FROM `newsletter` WHERE `email` LIKE '$email'");
		if ($result->num_rows == 1) { $ERR["email"] = "Ez az email cím már regisztrálva van."; }
		
		if (count($ERR) == 0) {
			$res = $conn->query("INSERT INTO `newsletter` (`id`, `lastmail`, `date`, `name`, `email`, `is_allowed`, `sent`) VALUES (NULL, '0', CURRENT_TIMESTAMP, '$name', '$email', '1', '0');");
			$CNF->notify("$name has just subscribed.", 99);
			if (!$res) {
				$ERR["main"] = "Nem sikerült rögzíteni az adatokat!";
			} else {
				echo $conn->insert_id;
				echo "sikeres feliratkozas";
			}
		}
	}

	if (count($ERR) > 0 || !isset($_POST["email"])) {
		?>
		<div class="container">
			<div class="row" id="subscribe">
				<h1 class="col s12 center-align">E-mail értesítés</h1>
				<?php if (!isset($_SESSION["subscribe"])) { ?>
				<form class="" autocomplete="off" action="hirlevel/feliratkozas" method="post">
					<div class="col s12 m6">
						<h5 style="margin: 10px 0;" class="col s12">Az Ön adatai</h5>
						<?php if (isset($ERR["main"])) echo '<p class="red-text col s12">'.$ERR["main"].'</p>'; ?>
						<div class="input-field col s12">
							<input id="name" type="text" name="name" class="validate" value="<?php if (isset($_POST["name"])) echo $_POST["name"]; ?>">
							<label for="name">Teljes név</label>
							<?php if (isset($ERR["name"])) echo '<p class="red-text no-margin">'.$ERR["name"].'</p>'; ?>
						</div>
						<div class="input-field col s12">
							<input id="email" type="email" name="email" class="validate" value="<?php if (isset($_POST["name"])) echo $_POST["email"]; ?>"> 
							<label for="email">E-mail cím</label>
							<?php if (isset($ERR["email"])) echo '<p class="red-text no-margin">'.$ERR["email"].'</p>'; ?>
						</div> 
					</div> 
					<div class="col s12 m6">
						<h5 style="margin: 10px 0;">Tudnivalók</h5>
						<p class="flow-text" style="margin: 0;">Hírlevelünkben hetente értesülhet az éppen aktuális napi menü ajánlatainkról! </p>
						<p style="margin: 10px 0;">Adatait biztonságban kezeljük, valamint nem adjuk ki azokat harmadik félnek.</p>
					</div> 
					<div class="input-field col s12 center-align">
						<button class="btn waves-effect waves-light primary-color" type="submit" name="action"> Feliratkozás hírlevélre <i class="material-icons right">send</i></button>
					</div>
				</form>
				<?php } else { echo '<p class="flow-text col s12">Sikeresen előrendelte a könyvet! E-mail cím: '.$_SESSION["shizoo"].'</p>'; } ?> 
			</div>
		</div>
		<?php
	} 
	// Display form on error / direct access
	
} else { ?>
<div class="container">
	<div class="row">
		<div class="col m3 hide-on-small-only"></div>
		<div class="col s12 m6">
			<div class="card">
				<div class="card-content row">
					<span class="card-title col s12">Regisztrációkor megadott e-mail cím</span>
					<form action="hirlevel" method="post" enctype="multipart/form-data">
					    <div class="input-field col s12">
							<input id="email" name="email" type="email" class="validate">							
							<label for="email" data-error="Helytelen e-mail cím" data-success="Helyes e-mail cím">E-mail cím</label>
						</div>	
					    <div class="input-field col s12">
							<button class="btn waves-effect waves-light primary-color wide" type="submit" name="submit">Leiratkozás</button>
							<br>
							<br>
							<span class="">Amennyiben problémája akad a leiratkozással, kérjük keressen fel elérhetőségeinken!</span>
							<br>
						</div>	
					</form>
				</div>
          </div>
		</div>
		<div class="col m3 hide-on-small-only"></div>
	</div>
</div>
<?php
	// Unsub
}
?>