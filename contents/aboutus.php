<div class="container">
    <div class="section">
		<?php $history = $page->text("tortenetunk");?>
		<div class="row">
			<div class="col s12 center">
				<h2 class="bold"><?php echo $history["title"]; ?></h2>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m4 push-m8">
				<img src="images/aboutus.jpg" class="responsive z-depth-1">
			</div> 
			<div class="col s12 m8 pull-m4">
				<p class="flow-text no-margin">
					<?php echo $history["content"]; ?>
				</p>
			</div>
		</div>
	</div>
</div> 