<div class="container">
	<div class="row">
        <div class="col s12">
			<h1 class="thin">Adatvédelmi Tájékoztató</h1>
			<div class="card white black-text">
				<div class="card-content">
					<span class="card-title">A Szolgáltató</span>
					<p>Jelen Adatvédelmi Tájékoztató összefoglalja a hernadi-antikvarium.hu kezelője által folytatott adatkezelési elveket és annak gyakorlatát.<br/> A látogató/felhasználó az adatok elküldésével kijelenti, hogy azok a valóságnak megfelelnek.</p>
				</div>
				<div class="card-content">
					<span class="card-title">1. ADATKEZELŐ</span>
					<p>Az adatok kezelője a Kedvenc Könyv Kft (továbbiakban: adatkezelő).<br/>Székhely/postázási cím: 1035 Budapest, Szentendrei út 8.<br/>Cégjegyzékszám: 01-09-925365<br/></p>
				</div>
				<div class="card-content">
					<span class="card-title">2. A KEZELT ADATOK ÉS ÉRINTETTEK KÖRE</span>
					<p>A weboldal látogatása során anonim módon a látogató session ID-t kap, amellyel a Google Analytics a személyes adatoktól függetlenül, statisztikai céllal rögzíti a látogatás kezdő- illetve befejező időpontját, illetve a látogatott oldalakat. Erről a gyakorlatról minden új látogató az első oldalletöltés alkalmával tájékoztatásban részesül.</p>
					<p>A weboldalon a látogató regisztráció vagy rendelés ürlap kitöltése formájában önkéntesen hozzájárulhat a következő személyes adatainak kezeléséhez:</p>
					<ul>
						<li>teljes név</li>
						<li>telefonszám</li>
						<li>e-mail cím</li>
						<li>számlázási cím (számlázási név, utcanév, házszám, település, irányítószám),</li>
						<li>szállítási cím (szállítási név, utcanév, házszám, település, irányítószám),</li>
					</ul>
					<p>A következő pontban meghatározott célokra megadott adatok kezelése a felhasználó önkéntes hozzájárulásával történik.</p>
					<p>A felhasználó által megadott adatok pontatlanságáért, vagy valótlanságáért az adatkezelő felelősséget nem vállal.</p>
				</div>
				<div class="card-content">
					<span class="card-title">3. AZ ADATKEZELÉS CÉLJA</span>
					<p>A személyes adatok kezelésének célja</p>
					<ul>
						<li>a felhasználóval való kapcsolattartás;</li>
						<li>a felhasználó azonosítása, a többi felhasználótól való megkülönböztetése;</li>
						<li>felhasználói elégedettség tesztelése és mérése;</li>
						<li>az e-mail hírlevélre feliratkozók számára heti hírlevél értesítések küldése.</li>
					</ul>
					<p>A felhasználó leiratkozni a “Leiratkozás” gomb/felirat megnyomásával tud az e-mailben, időtől függetlenül.</p>
					<p>A Google Analytics által használt anonim session ID-k és cookiek (sütik) felhasználásának célja statisztikai jellegű. A felhasználó a regisztráció során önként hozzájárul, hogy a 2. pontban említett típusú adatokat az adatkezelő az Adatvédelmi Tájékoztató rendelkezéseinek megfelelően felhasználja. Az adatkezelő a megadott adatokat bizalmasan kezeli.</p>
				</div>
				<div class="card-content">
					<span class="card-title">4. AZ ADATVÉDELMI TÁJÉKOZTATÓ ÁLTAL MEGHATÁROZOTT ADATKEZELÉSI IDŐTARTAM</span>
					<p>A Google Analytics által létrehozott cookiek időtartama 30 napra szól, a session ID-k az oldal elhagyásakor automatikusan törlődnek. A felhasználó által megadott adatokat a felhasználó írásos kezdeményezésére az adatkezelő 10 munkanapon belül törli. Ezt az igényt az hernadi.antikvarium(kukac)gmail.com e-mail címen lehet bejelenteni.</p>
				</div>
				<div class="card-content">
					<span class="card-title">5. AZ ADATFELDOLGOZÓK KÖRE</span>
					<p>A Társaság a weboldal folyamatos és megfelelő működőképességének biztosítása, a megrendelések teljesítése, illetve egyéb, a webáruházi szolgáltatások biztosításához szorosan kapcsolódó tevékenységek elvégzése céljából adatfeldolgozót vehet igénybe.</p>
					<p>A társaság által igénybe vett adatfeldolgozók megnevezése: </p>
					<table border="1" cellspacing="0" cellpadding="0">
						<tbody>
							<tr>
								<td><b>Cégnév</b></td>
								<td><b>Székhely</b></td>
								<td><b>Tevékenység</b></td>
							</tr>
							<tr>
								<td><p>Magyar Posta Zrt</p></td>
								<td><p>1138 Budapest, Dunavirág utca 2-6. </p></td>
								<td><p>Csomagszállítás</p></td>
							</tr>
							<tr>
								<td><p>Foxpost Zrt. </p></td>
								<td><p>3200 Gyöngyös, Batsányi János utca 9. </p></td>
								<td><p>Csomagszállítás</p></td>
							</tr>
							<tr>
								<td><p>SPRINTER Futárszolgálat Kft. </p></td>
								<td><p>1097 Budapest, Táblás utca 39. </p></td>
								<td><p>Csomagszállítás</p></td>
							</tr>
							<tr>
								<td><p>DPD Hungária Kft.</p></td>
								<td><p>1158 Budapest, Késmárk utca 14/B </p></td>
								<td><p>Csomagszállítás</p></td>
							</tr>
							<tr>
								<td><p>Dream Interactive Kft.</p></td>
								<td><p>1027 Budapest, Medve utca 24.</p></td>
								<td><p>Ügyfélkommunikáció</p></td>
							</tr>
						</tbody>
					</table>
					<p>Az adatkezelő a fent említett eseteken kívül egyedül törvényi kötelességből továbbítja a felhasználók adatait az igénylő hatóságnak, amennyiben hivatkozott hatóság a megfelelő jogalappal rendelkezik.</p>
				</div>
				<div class="card-content">
					<span class="card-title">6. AZ ADATKEZELÉSI SZABÁLYZAT JOGALAPJA ÉS A FELHASZNÁLÓ JOGAI</span>
					<p>Az adatkezelés a felhasználó jóváhagyásának jogalapjával történik. A felhasználó az adatkezelő által kezelt adatokról, és annak módjáról a hernadi.antikvarium(kukac)gmail.com e-mail címen ezen Adatvédelmi Tájékoztató tartalmán túl további tájékoztatást kérhet. A felhasználó által megadott adatokat a felhasználó írásos kezdeményezésére az adatkezelő 10 munkanapon belül törli. Ezt az igényt a hernadi.antikvarium(kukac)gmail.com e-mail címen lehet bejelenteni.</p>
				</div>
				<div class="card-content">
					<span class="card-title">7. A WEBOLDALT ÜZEMELTETŐ PARTNER ÉS TECHNOLÓGIA</span>
					<p>A hernadi-antikvarium.hu weboldalt Bencsik György Gábor EV. üzemelteti. </p>
				</div> 
				<div class="card-content">
					<span class="card-title">8. JOGORVOSLATI LEHETŐSÉG ÉS HATÁLYOS TÖRVÉNYEK</span>
					<p>Amennyiben a felhasználó úgy érzi, hogy az Seven Digits Media Kft. a személyes adatokat nem az itt megjelöltek szerint vagy a hatályos törvénynek megfelelően kezeli, jogait az adatvédelmi biztosnál vagy polgári bíróságon érvényesítheti, továbbá a Nemzeti Adatvédelmi és Információszabadság Hatósághoz fordulhat.</p>
					<p>A adatvédelemmel kapcsolatos kötelezettségekről az alábbi törvényekben tájékozódhat a felhasználó:</p>
					<p>A személyes adatok védelméről és a közérdekű adatok nyilvánosságáról szóló 1992. évi LXIII. törvény</p>
					<p>Az egyének védelméről a személyes adatok gépi feldolgozása során szóló 1998. évi VI. törvény</p>
					<p>Az információs önrendelkezési jogról és az információszabadságról szóló 2011. évi CXII. törvény</p>
				</div>
				
				
				
				








				<div class="card-action">
				  <p>Hatályos: 2016. április 22-től visszavonásig és/vagy módosításáig. Rendelkezéseit csak a hatálybalépését követően kötött szerződésekre kell alkalmazni, korábbi szerződésekre a hatályba lépést megelőző általános szerződési feltételek rendelkezései az irányadóak.</p>
				</div>
			</div>
        </div>
    </div>
</div>
	
