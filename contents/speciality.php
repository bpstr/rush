<?php 

if ($todaySpecial) {
	$anchor = $todaySpecial["date"];
?>
<div class="container" id="todaySpecial">
    <div class="section">
		<div class="row">
			<div class="col s12 m3 push-m9">
				<img src="images/daily/<?php echo $todaySpecial["image"]; ?>" class="z-depth-1 responsive materialboxed" alt="NemPiskóta Étkezde napi ajánlat">
			</div>
			<div class="col s12 m7 pull-m3">
				<?php $speciality = $page->text("mai-kulonlegesseg");?>
				<h4 class="bold"><?php echo $CNF->ago($todaySpecial["date"]); ?></h4>
				<h2 class="thin"><?php echo $todaySpecial["name"]; ?></h2>
				<p class="flow-text"><?php echo $todaySpecial["description"]; ?></p>
				<p class="divider"></p>
				<div class="fb-like" data-href="http://nempiskota.eu/kulonlegesseg/<?php echo $todaySpecial["id"]; ?>/<?php echo $todaySpecial["uri"]; ?>" data-layout="standard" data-action="recommend" data-size="large" data-show-faces="false" data-share="true"></div>
			</div>
		</div>
	</div>
</div> 
<div class="divider"></div> 
<?php } ?>
<div class="container">
    <div class="section">
		<div class="row">
			<h2 class="col s12 thin">Korábbi ajánlataink...</h2>
			<?php 
				foreach($page->fetch("SELECT * FROM daily_offer WHERE `date` < '".date("Y-m-d H:i:s", strtotime($anchor))."' ORDER BY `date` DESC
 LIMIT 0,3") as $row) {
					echo '<div class="col s12 m4">';
					echo '<div class="card medium">';
					echo '<div class="card-image">';
					echo '<img src="images/daily/'.$row["image"].'" alt="NemPiskóta Étkezde: '.$row["name"].'" class="materialboxed">';
					echo '<span class="card-title semi-transparent truncate " style="padding: 10px 15px">'.ucfirst($CNF->months[date("n", strtotime($row["date"]))]).' '.date("j", strtotime($row["date"])).'. '.$CNF->days[date("l", strtotime($row["date"]))].'</span>';
					echo '</div>';
					echo '<div class="card-content">';
					echo '<a href="kulonlegesseg/'.$row["id"].'/'.$row["uri"].'" class="card-title">'.$row["name"].'</a>';
					echo '<p>'.$row["description"].'</p>';
					echo '</div>';
					echo '</div>';
					echo '</div>';
				}
			?>
		</div>
	</div>
</div> 