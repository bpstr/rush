<!-- Masthead -->
<?php $article = $page->text("masthead");?>
<section class="my-5 pt-5">
    <div class="container-float">
        <div class="row m-0">
            <div class="col-lg-3 col-md-4 p-0 offset-lg-1"><img src="images/articles/cropped-<?= $article['images'][0]['src']; ?>" class="w-100"></div>
            <div class="col-lg-7 p-0 offset-md-1 col-md-7 pt-5 my-auto"><h1 class="display-4 pb-5 font-weight-bold editable"><?= $article['title']; ?></h1></div>
        </div>
        <div class="row m-0">
            <div class="col-lg-5 p-0"><p class="lead mt-n5 ml-5"><?= $article['content']; ?></p></div>
            <div class="col-lg-2 p-0 offset-md-4 d-md-none d-lg-block order-md-first"><img src="images/articles/cropped-<?= $article['images'][1]['src']; ?>" class="w-100"></div>
        </div>
    </div>
</section>

<!-- Team -->
<?php $article = $page->text("team");?>
<section class="my-5 pt-5">
    <div class="container-fluid">
        <div class="row m-0">
            <div class="col-xl-4 col-lg-4 col-md-5 offset-xl-1">
                <h2 class="pb-2 font-weight-bold"><?= $article['title']; ?></h2>
                <h5 class="pb-3 font-weight-bold"><?= $article['subtitle']; ?></h5>
                <p class=""><?= $article['content']; ?></p>
            </div> 
            <div class="col-xl-6 p-0 offset-md-1 col-md-7 pt-5 my-auto">
                <img src="<?= $article['images'][0]['src']; ?>" class="w-100">
            </div>
        </div>
    </div>
</section>

<!-- Highlights -->
<?php $article = $page->text("highlights");?>
<section id="portfolio" class="portfolio">
    <div class="container-fluid p-0">
        <div class="row no-gutters">
			<?php
			foreach ($page->fetch('SELECT * FROM `krav__images` WHERE GID = 1') as $image) {
				echo '<div class="col-lg-2 col-md-4 col-6">';
				$page->printImage($image);
				echo '</div>';
			}
			?>
        </div>
    </div>
</section>


<section class="my-5">
    <div class="container-fluid">
		<div class="row no-gutters">
			<?php
			foreach ($article['images'] as $image) {
				echo '<div class="col-lg-2 col-md-4 col-6">';
				$page->printImage($image);
				echo '</div>';
			} 
			?>
		</div>
        <div class="row mt-5">
            <div class="col-md-10 col-lg-6 p-0 offset-md-1">
                <div class="row mt-5 mx-0">
                    <div class="col-xs-12 col-sm-8 my-auto"><h1 class="font-weight-bold display-4"><?= $article['subtitle']; ?></h1></div>
                </div>
                <div class="row mx-0"> 
                    <p class="lead col"><?= $article['content']; ?></p></div>
            </div>
            <div class="col-lg-5 col-md-9 offset-md-3 offset-lg-0">
                <h1 class="display-4 font-weight-bold text-right m-5"><?= $article['title']; ?></h1>
            </div>
        </div>
    </div>
</section>

<section class="my-5">
    <div class="container-fluid p-0">
        <div class="row pt-5 m-0">
            <div class="col-lg-4 p-0"><img src="http://placehold.it/400x600" class="w-100"></div>
            <div class="col-xxl-6 col-xl-7 col-lg-8 p-0 my-auto offset-xl-1 p-3 ">
                <h1 class="display-4 font-weight-bold mb-5">Krav-Maga edzés felnőtt kamasz vegyes csoport kezdőknek és haladóknak</h1>
                <h4 class="mt-5 " class="text-primary">Lorem diplsum</h4>
                <p class="lead m-5">IMI LICHTENFELD 1910-ben született Budapesten és Pozsonyban nőtt fel, Szlovákia fővárosában. Imi fejlődésére nagy hatással volt édesapja, Samuel Lichtenfeld. 13 éves korától Samuel egy vándorcirkuszban birkózott és mutatott be súlyemelő gyakorlatokat, és ezt az atlétika iránti érdeklődést átadta Iminek. 20 évvel később Samuel belépett a szlovák rendőrségbe, ahol főfelügyelői rangra emelkedett</p>
                <h4 class="mt-5" class="text-primary">Lorem diplsum</h4>
                <p class="lead m-5 mb-0">IMI LICHTENFELD 1910-ben született Budapesten és Pozsonyban nőtt fel, Szlovákia fővárosában. Imi fejlődésére nagy hatással volt édesapja, Samuel Lichtenfeld. 13 éves korától Samuel egy vándorcirkuszban birkózott és mutatott be súlyemelő gyakorlatokat, és ezt az atlétika iránti érdeklődést átadta Iminek. 20 évvel később Samuel belépett a szlovák rendőrségbe, ahol főfelügyelői rangra emelkedett</p>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid p-0">
        <div class="row pt-5 m-0">
            <div class="col-lg-4"><h1 class="display-4 font-weight-bold mb-5">Krav-Maga edzés felnőtt kamasz vegyes csoport kezdőknek és haladóknak</h1></div>
            <div class="col-md-6 col-lg-4 my-auto"><p class="lead mx-md-5 mb-0">IMI LICHTENFELD 1910-ben született Budapesten és Pozsonyban nőtt fel, Szlovákia fővárosában. Imi fejlődésére nagy hatással volt édesapja, Samuel Lichtenfeld. 13 éves korától Samuel egy vándorcirkuszban birkózott és mutatott be súlyemelő gyakorlatokat, és ezt az atlétika iránti érdeklődést átadta Iminek. 20 évvel később Samuel belépett a szlovák rendőrségbe, ahol főfelügyelői rangra emelkedett</p></div>
            <div class="col-md-6 col-lg-4 my-auto"><p class="lead mx-md-5 mb-0">IMI LICHTENFELD 1910-ben született Budapesten és Pozsonyban nőtt fel, Szlovákia fővárosában. Imi fejlődésére nagy hatással volt édesapja, Samuel Lichtenfeld. 13 éves korától Samuel egy vándorcirkuszban birkózott és mutatott be súlyemelő gyakorlatokat, és ezt az atlétika iránti érdeklődést átadta Iminek. 20 évvel később Samuel belépett a szlovák rendőrségbe, ahol főfelügyelői rangra emelkedett</p></div>
        </div>
    </div>
</section>
<!-- Masthead -->
<header>
    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col-xl-8 col-lg-7 col-12 masthead-background" style="background-image: url('images/articles/<?= $C('article.masthead.image'); ?>'); background-size: cover;">
                <img src="images/articles/<?= $C('article.masthead.image'); ?>" alt="<?= $C('article.masthead.title'); ?>" class="d-lg-none w-100">
            </div>
            <div class="col-xl-4 col-lg-5 mt-4 py-5 px-4 mb-5">
                <span class="text-muted font-italic font-serif pt-5"><?= $C('article.masthead.subtitle'); ?></span>
                <h2 class="font-serif display-5"><?= $C('article.masthead.title'); ?></h2>
                <hr class="divider my-4 mr-auto ml-0">
                <p class="lead"><?= $C('article.masthead.content'); ?></p>
                <a class="btn btn-primary js-scroll-trigger" href="<?= $C('article.masthead.forward'); ?>"><?= $C('article.masthead.button'); ?></a>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-sm-4 text-center bg-secondary text-light py-3">
                <h2 class="text-primary"><?= $C('icon.map'); ?></h2><h5 class="font-serif">Címünk</h5><p class="lead"><?= $C('meta.contact.region'); ?><br><?= $C('meta.contact.street'); ?></p>
            </div>
            <div class="col-sm-4 text-center bg-secondary text-light py-3">
                <div class=""><h2 class="text-primary"><?= $C('icon.clock'); ?></h2><h5 class="font-serif">Nyitvatartás</h5><p class="lead">Hétfő-péntek: <?= $C('meta.contact.weekdays'); ?><br>Szombat: <?= $C('meta.contact.weekend'); ?></p></div>
            </div>
            <div class="col-sm-4 text-center bg-secondary text-light py-3">
                <h2 class="text-primary"><?= $C('icon.phone'); ?></h2><h5 class="font-serif">Kapcsolat</h5><p class="lead"><?= $C('meta.contact.email'); ?><br><?= $C('meta.contact.phone'); ?></p>
            </div>
        </div>
    </div>
</header>



<!-- Discover -->
<section class="section py-5" id="discover">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-6 align-self-start">

                <hr class="divider border-secondary mt-0 mb-4 mr-auto ml-n3">
                <p class="lead"><?= $C('article.about.subtitle'); ?></p>

                <img src="images/azta.jpeg" class="img-fluid mt-5 d-md-none d-lg-block">
            </div>
            <div class="col-lg-4 col-md-3 col-6 align-self-start">

                <div class="shifted-border">
                    <img src="images/articles/<?= $C('article.about.image'); ?>" class="img-fluid">
                </div>

            </div>
            <div class="col-lg-6 col-md-6 align-self-center py-3">
                <!-- Content -->
                <h2 class="font-serif"><?= $C('article.about.title'); ?></h2>
                <p class="mb-5 "><?= $C('article.about.content'); ?></p>
            </div>
        </div>
    </div>
</section>



<!-- Menu Section -->
<section class="my-5 position-relative" id="menu">
    <div class="bg-light pt-5 mb-3 pb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 text-center">
                    <h2 class="display-4"><?= $C('article.menu.title'); ?></h2>
                    <hr class="divider border-primary my-4">
                    <p class="lead font-serif"><?= $C('article.menu.content'); ?></p>
                </div>
            </div>
        </div>
    </div>
    <img src="/assets/logo.svg" class="align-top position-absolute w-25" alt="" style="transform: rotate(-15deg); top: -100px; left: 10px; opacity: 0.66;">
    <div class="container">
        <div class="row">
        </div>
    </div>
</section>

<section id="portfolio" class="portfolio">
    <div class="container-fluid p-0">
        <div class="row no-gutters">
        </div>
    </div>
</section>




<!-- About Section -->
<section class="my-5" id="about">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 offset-lg-1 col-md-6 align-self-center">
                <h2 class="font-serif"><?= $C('article.restaurant.title'); ?></h2>
				<?php
				$content = $C('article.restaurant.content');
				$paragraphs = explode("<br>", $content);
				foreach($paragraphs as $paragraph) {
					echo '<p>'.$paragraph.'</p>';
				}
				?>
            </div>
            <div class="col-md-3 col-7 ml-4 align-self-start">
                <div class="shifted-border">
                    <img src="images/articles/scaled-<?= $C('article.restaurant.image'); ?>" class="img-fluid" alt="<?= strip_tags($C('article.restaurant.title')); ?>">
                </div>
            </div>
            <div class="col-md-3 mt-5 ml-n5 col-5 align-self-start offset-md-1">
                <div class="shifted-border">
                    <img src="images/aztapasta.jpeg" class="img-fluid" alt="<?= strip_tags($C('article.restaurant.title')); ?>">
                </div>
            </div>
        </div>
    </div>
</section>

<section id="portfolio2" class="portfolio">
    <div class="container-fluid p-0">
        <div class="row no-gutters">
			<?php
			$colmatrix = [
				'col-lg-2 col-md-4 col-6 ml-5',
				'col-lg-2 col-md-4 col-5 ml-n5 mr-n5 mt-n3',
				'col-lg-2 col-md-4 col-6 ml-3 mr-n3 mt-5',
				'col-lg-3 col-md-4 col-6 mt-n3 mr-n5 offset-md-1 offset-lg-0',
				'col-lg-2 col-md-4 col-7 mt-3 mr-n5 ',
				'col-lg-2 col-md-4 col-5 mt-n5 ml-n5',
			];
			?>
        </div>
    </div>
</section>


<!-- Delivery Section -->
<section class="my-5" id="delivery">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-12 mb-5 align-self-start">
                <div class="shifted-border">
                    <img src="images/articles/cropped-<?= $C('article.delivery.image'); ?>" class="img-fluid" alt="<?= strip_tags($C('article.delivery.title')); ?>">
                </div>
            </div>
            <div class="col-md-7 col-12 mt-5">
                <h2 class="font-serif"><?= $C('article.delivery.title'); ?></h2>
                <h5 class="font-weight-light"><?= $C('article.delivery.subtitle'); ?></h5>
                <hr class="divider my-4 mr-auto ml-0">
				<?php
				$content = $C('article.delivery.content');
				$paragraphs = explode("<br>", $content);
				foreach($paragraphs as $paragraph) {
					echo '<p>'.$paragraph.'</p>';
				}
				?>
            </div>
        </div>
    </div>
</section>
