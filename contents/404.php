<div id="index-banner" class="parallax-container">
	<div class="section no-pad-bot">
		<div class="container">
			<div class="row">
			  <h1 class="col s12 thin center-align" style="font-size: 120px;">404</h1>
			  <h4 class="col s12 center-align">A keresett oldal nem található!</h4>
			</div>   
		</div>   
	</div>
	<div class="parallax"><img src="images/page/cover.jpg"></div>
</div> 


  